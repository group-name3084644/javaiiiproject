# JavaIIIProject

## Project status
Completed
## Name
Employee/Admin Application
## Description
This is a project for Java III. It includes an admin app for managing users and employees.
Admin can make orders, manage users, and products. While Employees can make a shopping cart for customers that come in to buy from the store. 
Customers can have bonus points that will be accumatated from buying products from our store such as meat or fruits.
Once the customer acquire points, the points can be used to obtain products at a reduce rate.
## Installation
To install the project, clone the repository and import it into your preferred IDE.
runs on java 1.19 and uses maven 
git clone https://gitlab.com/group-name3084644/javaiiiproject.git this REPO on git bash in your desire folder 
 Run your IED and then hit RUN for AdminAppl.java for the admin app or hit RUN for the EmployeeAppl.java for the employee app.

## Authors

- **Danny** - complete work - [JohnDoe](https://gitlab.com/danny.hoa09)
- **Alex** - complete - [JaneSmith](https://gitlab.com/alexroxas7)

See also the list of [contributors](https://gitlab.com/group-name3084644/javaiiiproject/graphs/main) who participated in this project.


## Support
Please message me at https://gitlab.com/danny.hoa09 OR https://gitlab.com/alexroxas7 for more questions
or my mio if you're affiliated with Dawson College(Danny Hoang)
## Roadmap
For now the project is done but maybe in the near future this app can have additional features.

## Contributing
closed for personal contributions only between group members of this project.

## Authors and acknowledgment
Alex Roxas thank you, thank you.
## License

This project is licensed under the GNU General Public License v3.0 - see the [[LICENSE.md](https://choosealicense.com/licenses/gpl-3.0/)](LICENSE.md) file for details.