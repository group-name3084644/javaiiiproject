CREATE TABLE Customer (
    CUSTOMER_ID NUMBER GENERATED ALWAYS AS IDENTITY,
    TYPENAME VARCHAR2(50),    
    NAME VARCHAR2(50),
    CONTACT_INFO VARCHAR2(100),
    STORE_POINTS NUMBER
);

CREATE TABLE Products (
    PRODUCT_ID NUMBER GENERATED ALWAYS AS IDENTITY,
    TYPENAME VARCHAR2(50),
    NAME VARCHAR2(50),
    QUANTITY_EACH NUMBER(10,2),
    PRICE NUMBER(10,2),
    BRAND VARCHAR2(50),
    STOCK NUMBER(10),
    STORE_POINTS NUMBER(10),
    SALE NUMBER(10)
);

--user TABLE
CREATE TABLE Users (
    USER_ID NUMBER GENERATED ALWAYS AS IDENTITY,
    USER_TYPE VARCHAR2(50),
    USERNAME VARCHAR2(50),
    PASSWORD VARCHAR2(50)
);


INSERT INTO Customer (TYPENAME, NAME, CONTACT_INFO, STORE_POINTS) VALUES ('Customer', 'John', '514-515-1515', 100);
INSERT INTO Customer (TYPENAME, NAME, CONTACT_INFO, STORE_POINTS) VALUES ('Customer', 'Mike', '514-515-1525', 670);
INSERT INTO Customer (TYPENAME, NAME, CONTACT_INFO, STORE_POINTS) VALUES ('Customer', 'Bob', '514-515-1535', 400);


INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Fruits', 'Apple', 2,  1.50, 'Red Delicious', 200, 50, 667);
INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Fruits', 'Banana', 1, 1.00, 'Chiquita', 150, 45, 200);
INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Fruits', 'Orange', 3, 1.25, 'Florida', 320, 30, 325);
INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Meat', 'Chicken',1,  5.00, 'Tyson', 80, 200, 52);
INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Meat', 'Beef', 1, 6.00, 'Angus', 50, 300, 75);
INSERT INTO Products ( TYPENAME, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES ('Meat', 'Pork', 1,  4.00, 'Smithfield', 40, 200, 54);
--make some user inserts examples one for admin one for employee

INSERT INTO Users (USER_TYPE, USERNAME, PASSWORD) VALUES ('Admin','admin', 'admin123');
INSERT INTO Users (USER_TYPE, USERNAME, PASSWORD) VALUES ('Employee','employee', 'employee123');

--function package for callable statements 
CREATE OR REPLACE PACKAGE my_package AS
    TYPE ref_cursor IS REF CURSOR;
    FUNCTION getProduct RETURN ref_cursor;
    FUNCTION getCustomer RETURN ref_cursor;
    FUNCTION getEmployee RETURN ref_cursor;
END my_package;
/

--function package body for callable statements 
CREATE OR REPLACE PACKAGE BODY my_package AS
    FUNCTION getProduct RETURN ref_cursor IS
        my_cursor ref_cursor;
    BEGIN
        OPEN my_cursor FOR
        SELECT * FROM Products;
        RETURN my_cursor;
    END getProduct;

    FUNCTION getCustomer RETURN ref_cursor IS
        my_cursor ref_cursor;
    BEGIN
        OPEN my_cursor FOR
        SELECT * FROM Customer;
        RETURN my_cursor;
    END getCustomer;

    FUNCTION getEmployee RETURN ref_cursor IS
        my_cursor ref_cursor;
    BEGIN
        OPEN my_cursor FOR
        SELECT * FROM Users WHERE USER_TYPE = 'Employee';
        RETURN my_cursor;
    END getEmployee;
END my_package;

--procedure package for callable statements
CREATE OR REPLACE PACKAGE my_package2 AS
    PROCEDURE updatePoints (customer_id NUMBER, points NUMBER);
    PROCEDURE addCustomer (customer_id NUMBER, name VARCHAR2, contact_info VARCHAR2, store_points NUMBER);
    PROCEDURE addProduct (category VARCHAR2, name VARCHAR2, quantity_each NUMBER, price NUMBER, brand VARCHAR2, stock NUMBER, store_points NUMBER, sale NUMBER);
    PROCEDURE addEmployee (user_id NUMBER, username VARCHAR2, password VARCHAR2);
    PROCEDURE updateInventory(product_id NUMBER, stock NUMBER);
    PROCEDURE deleteProduct(product_id NUMBER);
    PROCEDURE deleteEMPLOYEE(user_id NUMBER);
END my_package2;
/

--procedure package body for callable statements
CREATE OR REPLACE PACKAGE BODY my_package2 AS
    PROCEDURE updatePoints (customer_id NUMBER, points NUMBER) IS
    BEGIN
        UPDATE Customer SET STORE_POINTS = STORE_POINTS + points WHERE CUSTOMER_ID = customer_id;
    END updatePoints;

    PROCEDURE addCustomer (customer_id NUMBER, name VARCHAR2, contact_info VARCHAR2, store_points NUMBER) IS
    BEGIN
        INSERT INTO Customer (CUSTOMER_ID, NAME, CONTACT_INFO, STORE_POINTS) VALUES (customer_id, name, contact_info, store_points);
    END addCustomer;

    PROCEDURE addProduct (category VARCHAR2, name VARCHAR2, quantity_each NUMBER, price NUMBER, brand VARCHAR2, stock NUMBER, store_points NUMBER, sale NUMBER) IS
    BEGIN
        INSERT INTO Products (TYPENAME, CATEGORY, NAME, QUANTITY_EACH, PRICE, BRAND, STOCK, STORE_POINTS, SALE) VALUES (category, name, quantity_each, price, brand, stock, store_points, sale);
    END addProduct;

    PROCEDURE addEmployee (user_id NUMBER, username VARCHAR2, password VARCHAR2) IS
    BEGIN
        INSERT INTO Users (USER_ID, USERNAME, PASSWORD) VALUES (user_id, username, password);
    END addEmployee;

    PROCEDURE updateInventory(product_id NUMBER, stock NUMBER) IS
    BEGIN
        UPDATE Products SET STOCK = STOCK + stock WHERE PRODUCT_ID = product_id;
    END updateInventory;

    PROCEDURE deleteProduct(product_id NUMBER) IS
    BEGIN
        DELETE FROM Products WHERE PRODUCT_ID = product_id;
    END deleteProduct;

    PROCEDURE deleteEMPLOYEE(user_id NUMBER) IS
    BEGIN
        DELETE FROM Users WHERE USER_ID = user_id;
    END deleteEMPLOYEE;
END my_package2;
