package inventory;

import org.junit.Test;
import inventory.Food.Fruits;
import static org.junit.Assert.*;
import java.io.IOException;

public class FruitTest {
    @Test
    public void TestingFruitContructor() {
        try {
            Fruits fruit = new Fruits("Apple", -1, null, 11, 100, "vlone", 0, 0, 0);
            fail("This was not suppose to print");
            // TODO Auto-generated catch block
        } catch (Exception e) {

        }

    }

    @Test
    public void TestingfruitGetter() {
        try {
            Fruits fruit = new Fruits("Apple", 0, null, 11, 100, "vlone", 0, 0, 0);
            assertEquals("Apple", fruit.getName());
            assertEquals(11, fruit.getQuantityEach());
            assertEquals(100, fruit.getPrice(), 0);
            assertEquals("vlone", fruit.getBrand());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void TestingfruitSetter() {
        try {
            Fruits fruit = new Fruits("Apple", 0, null, 11, 100, "vlone", 0, 0, 0);
            fruit.setName("greenApple");
            assertEquals("greenApple", fruit.getName());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
