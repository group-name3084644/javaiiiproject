
package inventory;

import org.junit.Test;

import inventory.Display.AdminDisplay;
import inventory.Food.Fruits;
import inventory.Food.iFood;
import inventory.Sort.iSorter;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

public class AdminDisplayTest {
    @Test
    public void testSelectionSort2AndSwap() {
        try {
            // Create a list of Fruits objects for testing
            iFood fruits = new Fruits("string", 1, "Apple", 0, 1.0, "string", 10, 0, 0);
            iFood fruits2 = new Fruits("string", 2, "Banana", 0, 0.5, "string", 20, 0, 0);
            iFood fruits3 = new Fruits("string", 3, "Cherry", 0, 2.0, "string", 5, 0, 0);
            List<iFood> foodList = Arrays.asList(fruits, fruits2, fruits3);

            // Create a sorter that sorts iFood objects by price in ascending order
            iSorter sorter = new iSorter() {
                @Override
                public int sortFoods(iFood food1, iFood food2) {
                    return Double.compare(food1.getPrice(), food2.getPrice());
                }
            };

            // Sort the food list
            AdminDisplay.selectionSort2(foodList, sorter);

            // Check if the food list is sorted correctly
            assertEquals(fruits2, foodList.get(0)); // Banana has the lowest price
            assertEquals(fruits, foodList.get(1)); // Apple has the next lowest price
            assertEquals(fruits3, foodList.get(2)); // Cherry has the highest price
        } catch (Exception e) {
            e.printStackTrace();
            fail("An unexpected exception was thrown");
        }
    }
}