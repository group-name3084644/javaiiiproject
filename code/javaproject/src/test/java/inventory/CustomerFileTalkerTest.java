package inventory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import inventory.Users.Customer;
import inventory.Users.CustomerFileTalker;

public class CustomerFileTalkerTest {

    @Test
    public void testCustomerFileloader() {
        try {
            // Create a list of users for testing
            List<Customer> customerList = new ArrayList<>();
            CustomerFileTalker ct = new CustomerFileTalker();
            customerList = ct.loadCustomers();
            assertEquals(1, customerList.get(0).getId());
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void TestCustomerFileloaderFail() {
        try {
            // Create a list of users for testing
            List<Customer> customerList = new ArrayList<>();
            CustomerFileTalker ct = new CustomerFileTalker();
            customerList = ct.loadCustomers();
            assertEquals(1, customerList.get(0).getId());
            fail();
        } catch (IOException e) {
            // pass
        } catch (Exception e) {
            // pass
        }
    }
}
