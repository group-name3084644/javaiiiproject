package inventory;

import org.junit.Test;
import inventory.Users.Customer;
import static org.junit.Assert.*;
import java.io.IOException;

public class CustomerTest {
    // make tests
    @Test
    public void TestingCustomerContructor() {
        try {
            Customer customer = new Customer("Customer", -1, null, null, 0);
            fail("This was not suppose to print");
            // TODO Auto-generated catch block
        } catch (Exception e) {

        }

    }

    @Test
    public void TestingCustomerGetter() {
        try {
            Customer customer = new Customer("Customer", 0, null, null, 0);
            assertEquals("Customer", customer.getTypeName());
            assertEquals(0, customer.getId());
            assertEquals(null, customer.getName());
            assertEquals(null, customer.getContactInfo());
            assertEquals(0, customer.getPoints());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void TestingCustomerSetter() {
        Customer customer = new Customer("Customer", 0, "Jake", "222-222-2222", 100);
        customer.setName("John");
        assertEquals("John", customer.getName());
    }
}
