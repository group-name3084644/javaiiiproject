package inventory;

import org.junit.Test;
import inventory.Users.Employee;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class EmployeeTest {
    @Test
    public void TestingEmployeeContructor() {
        try {
            Employee employee = new Employee("Employee", -1, null, null, null);
            fail("This was not suppose to print");
            // TODO Auto-generated catch block
        } catch (Exception e) {

        }

    }

    @Test
    public void TestingEmployeeGetter() {
        Employee employee = new Employee("Employee", 1, "Jake", "222-222-2222", "1234");
        assertEquals("Employee", employee.getTypeName());
        assertEquals(1, employee.getId());
        assertEquals("Jake", employee.getName());
        assertEquals("222-222-2222", employee.getContactInfo());
        assertEquals("1234", employee.getPassword());
    }

    @Test
    public void TestingEmployeeSetter() {
        Employee employee = new Employee("Employee", 1, "Jake", "222-222-2222", "1234");
        employee.setName("John");
        assertEquals("John", employee.getName());
    }

}
