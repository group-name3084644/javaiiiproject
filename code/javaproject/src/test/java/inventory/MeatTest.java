package inventory;

import org.junit.Test;
import inventory.Food.Meats;
import static org.junit.Assert.*;

public class MeatTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidName() {
        // Invalid Name
        new Meats("Meat", 1, "", 2, 100, "Brand", 200, 300, 400);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidWeight() {
        // Invalid Weight
        new Meats("Meat", 1, "Name", -2, 100, "Brand", 200, 300, 400);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPrice() {
        // Invalid Price
        new Meats("Meat", 1, "Name", 2, -5, "Brand", 200, 300, 400);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidBrand() {
        // Invalid Brand
        new Meats("Meat", 1, "Name", 2, 100, "", 200, 300, 400);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidStock() {
        // Invalid Stock
        new Meats("Meat", 1, "Name", 2, 100, "Brand", -1, 300, 400);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidPoints() {
        new Meats("Meat", 1, "Name", 2, 100, "Brand", 200, -1, 400);
    }

    @Test
    public void MeatGettersTest() {
        Meats meat = new Meats("Meat", 1, "Name", 2, 100, "Brand", 200, 300, 400);
        // Type
        assertEquals("Meat", meat.getTypename());
        // ProductId
        assertEquals(1, meat.getId());
        // Name
        assertEquals("Name", meat.getName());
        // Weight
        assertEquals(2.00, meat.getWeight(), 0);
        // Price
        assertEquals(100, meat.getPrice(), 0);
        // Brand
        assertEquals("Brand", meat.getBrand());
        // Stock
        assertEquals(200, meat.getStock());
        // Points
        assertEquals(300, meat.getStorePoints());
        // Sale
        assertEquals(400, meat.getSale());
    }

    @Test
    public void MeatSettersTest() {
        Meats meat = new Meats("Meat", 1, "Name", 2, 100, "Brand", 200, 300, 400);
        meat.setName("newMeat");
        assertEquals("newMeat", meat.getName());
        meat.setQuantityEach(5);
        assertEquals(5.00, meat.getWeight(), .0001);
        meat.setPrice(10.00);
        assertEquals(10.00, meat.getPrice(), 0);
        meat.setBrand("newBrand");
        assertEquals("newBrand", meat.getBrand());
        meat.setStock(201);
        assertEquals(201, meat.getStock());
        meat.setStorePoints(301);
        assertEquals(301, meat.getStorePoints());
        meat.setSale(401);
        assertEquals(401, meat.getSale());
    }

};
