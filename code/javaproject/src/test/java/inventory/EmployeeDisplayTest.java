package inventory;

import inventory.Food.*;
import inventory.Display.*;
import inventory.Users.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


public class EmployeeDisplayTest {
    
    @Test
    public void testFindFood() throws Exception {
        List<iFood> foodList = new ArrayList<iFood>();
        EmployeeDisplay ed = new EmployeeDisplay();
        Meats meat = new Meats("TypeName", 1, "name", 1, 1, "brand", 1, 1, 1);
        Fruits fruit = new Fruits("TypeName", 2, "name", 1, 1, "brand", 1, 1, 1);
        foodList.add(meat);
        foodList.add(fruit);

        iFood meatCompare = ed.findFood(foodList, 1);
        assertEquals(meat, meatCompare);

        iFood fruitCompare = ed.findFood(foodList, 2);
        assertEquals(fruit, fruitCompare);
    }


    @Test
    public void testFindCustomer() throws Exception {
        List<Customer> customerList = new ArrayList<Customer>();
        EmployeeDisplay ed = new EmployeeDisplay();
        Customer customer1 = new Customer("TypeName", 1, "name", "contactInfo", 1);
        Customer customer2 = new Customer("TypeName", 2, "name", "contactInfo", 1);
        customerList.add(customer1);
        customerList.add(customer2);

        Customer customer1Compare = ed.findCustomer(customerList, 1);
        assertEquals(customer1, customer1Compare);

        Customer customer2Compare = ed.findCustomer(customerList, 2);
        assertEquals(customer2, customer2Compare);
    }
    
}
