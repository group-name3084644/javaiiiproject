package inventory.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import inventory.Display.EmployeeDisplay;
import inventory.Food.*;
import inventory.History.HistoryFileManager;
import inventory.Sort.*;
import inventory.Users.*;

/**
 * Purpose: This class is used by the employees of the stores which displays all
 * avaliable products in the store
 * and can manage Customer Orders by taking their orders, validating them, and
 * adding the products they buy. Which
 * then the application displays the Amount needed to be paid by the customer.
 * 
 * It also reads and updates the files where the data are stored in.
 * 
 * @author Alexander Roxas
 * @version 12-02-2023
 * 
 */
public class EmployeeAppl {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        EmployeeDisplay ed = new EmployeeDisplay();

        List<iFood> foodList = new ArrayList<iFood>();
        foodImporter fi = new foodImporter();
        try {
            foodList = fi.ImportFood();
        } catch (Exception e) {
            System.out.println("Foods File: " + e);
        }

        List<Customer> customerList = new ArrayList<Customer>();
        CustomerFileTalker ct = new CustomerFileTalker();
        try {
            customerList = ct.loadCustomers();
        } catch (Exception e) {
            System.out.println("Customers File: " + e);
        }

        List<Employee> employeeList = new ArrayList<Employee>();
        EmployeeFiletalker et = new EmployeeFiletalker();
        try {
            employeeList = et.loadEmployees();
        } catch (Exception e) {
            System.out.println("Employees File: " + e);
        }

        List<String> history = new ArrayList<String>();
        HistoryFileManager hm = new HistoryFileManager();
        try {
            history = hm.loadHistory();
        } catch (Exception e) {
            System.out.println("History File: " + e);
        }

        boolean close = false;
        System.out.flush();
        System.out.print("\033[H\033[2J");
        /*
         * Initially, we were going to be doing a log in and display the user's
         * username.
         * But, we have decided to scrap the idea.
         */
        System.out.println(ed.greetUser("User"));
        do {
            boolean mainMenuValid = false;
            do {
                clearScreen(sc);
                System.out.println(ed.displayMainMenu());
                String mainMenuChoiceSTR = sc.nextLine();
                int mainMenuChoice = -1;
                try {
                    mainMenuChoice = Integer.parseInt(mainMenuChoiceSTR);
                } catch (NumberFormatException e) {
                    System.out.println("No words allowed. Please Try again.");
                    clearScreen(sc);
                }

                switch (mainMenuChoice) {
                    case -1:
                        break;
                    case 1:
                        // Display Foods
                        boolean displayFoodValid = false;
                        do {
                            clearScreen(sc);
                            System.out.println(ed.displayFoodMenu());
                            String sortFoodChoiceSTR = sc.nextLine();
                            int sortFoodChoice = -1;
                            try {
                                sortFoodChoice = Integer.parseInt(sortFoodChoiceSTR);
                            } catch (NumberFormatException e) {
                                System.out.println("No words allowed. Please Try again.");
                                clearScreen(sc);
                            }
                            switch (sortFoodChoice) {
                                case -1:
                                    break;
                                case 1:
                                    // Category
                                    clearScreen(sc);
                                    boolean displayFoodTypeValid = false;
                                    do {
                                        System.out.println(ed.displayFoodType());
                                        String foodTypeChoiceSTR = sc.nextLine();
                                        int foodTypeChoice = -1;
                                        try {
                                            foodTypeChoice = Integer.parseInt(foodTypeChoiceSTR);
                                        } catch (NumberFormatException e) {
                                            System.out.println("No words allowed. Please Try again.");
                                            clearScreen(sc);
                                        }

                                        switch (foodTypeChoice) {
                                            case -1:
                                                break;
                                            case 1:
                                                clearScreen(sc);
                                                System.out.println(ed.printAllProductByType(foodList, "Meat"));
                                                clearScreen(sc);
                                                break;
                                            case 2:
                                                clearScreen(sc);
                                                System.out.println(ed.printAllProductByType(foodList, "Fruit"));
                                                clearScreen(sc);
                                                break;
                                            case 0:
                                                displayFoodTypeValid = true;
                                                break;
                                            default:
                                                System.out.println("Invalid value, please try again");
                                                break;
                                        }
                                    } while (!displayFoodTypeValid);
                                    break;
                                case 2:
                                    // MostSales
                                    clearScreen(sc);
                                    iSorter showMostSale = new showMostSale();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, showMostSale));
                                    break;
                                case 3:
                                    // LessSales
                                    clearScreen(sc);
                                    iSorter showLessSale = new showLessSale();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, showLessSale));
                                    break;
                                case 4:
                                    // Highest Price
                                    clearScreen(sc);
                                    iSorter sortMostExpensive = new sortMostExpensive();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, sortMostExpensive));
                                    break;
                                case 5:
                                    // Cheapest Price
                                    clearScreen(sc);
                                    iSorter sortCheapest = new sortCheapest();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, sortCheapest));
                                    break;
                                case 6:
                                    // Alphahetical Ascending
                                    clearScreen(sc);
                                    iSorter sortAscAlpha = new sortAscAlpha();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, sortAscAlpha));
                                    break;
                                case 7:
                                    // Alphabetical Descending
                                    clearScreen(sc);
                                    iSorter sortDescAlpha = new sortDescAlpha();
                                    System.out.println(ed.displaySortedFoodMenu(foodList, sortDescAlpha));
                                    break;
                                case 0:
                                    // Go back
                                    displayFoodValid = true;
                                    break;
                                default:
                                    System.out.println("Invalid value, please try again");
                                    break;
                            }

                        } while (!displayFoodValid);
                        mainMenuValid = true;
                        break;
                    case 2:
                        // Manage Orders
                        clearScreen(sc);
                        boolean manageOrderValid = false;
                        boolean customerSubscription = false;
                        boolean enterOrders = true;
                        Customer currentCustomer = null;
                        do {
                            enterOrders = false;
                            printEqualLines();
                            System.out.println("Is your customer a part of our points service?");
                            System.out.println("Y/N/Exit");
                            String pointsService = sc.nextLine().toUpperCase();
                            switch (pointsService) {
                                case "Y":
                                    manageOrderValid = true;
                                    boolean customerIdValid = false;
                                    int customerIdChoice = 124;
                                    do {
                                        clearScreen(sc);
                                        customerIdValid = true;
                                        System.out.println(ed.displayManageCustomer(customerList));
                                        String customerIdChoiceSTR = sc.nextLine();
                                        try {
                                            customerIdChoice = Integer.parseInt(customerIdChoiceSTR);
                                            try {
                                                currentCustomer = ed.findCustomer(customerList, customerIdChoice);
                                            } catch (Exception e) {
                                                System.out.println(e);
                                                manageOrderValid = false;
                                                customerIdValid = false;
                                            }
                                        } catch (Exception e) {
                                            System.out.println("No words allowed. Please Try again.");
                                            customerIdValid = false;

                                        }
                                    } while (!customerIdValid);
                                    enterOrders = true;
                                    clearScreen(sc);
                                    customerSubscription = true;
                                    break;

                                case "N":
                                    clearScreen(sc);
                                    boolean pointsDecision = false;
                                    do {
                                        printEqualLines();
                                        System.out.println("Does your customer want to subscribe?");
                                        System.out.println("Y/N/Exit");
                                        String subscription = sc.nextLine().toUpperCase();
                                        switch (subscription) {
                                            case "Y":
                                                // Create new customer
                                                int newId = customerList.get(customerList.size() - 1).getId() + 1;
                                                boolean nameValid = false;
                                                String name;
                                                do {
                                                    nameValid = true;
                                                    printEqualLines();
                                                    System.out.println("What is the customer's name?");
                                                    name = sc.nextLine();
                                                    if (name.equals("")) {
                                                        System.out.println("Name can't be empty, please try again");
                                                        nameValid = false;
                                                    }
                                                } while (!nameValid);
                                                printEqualLines();
                                                System.out.println("What is your customers mobile phone?");
                                                String contactInfo = sc.nextLine();
                                                Customer newCustomer = new Customer("Customer", newId, name,
                                                        contactInfo, 0);
                                                customerList.add(newCustomer);
                                                try {
                                                    ct.writeToCSV(newCustomer);
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                                currentCustomer = newCustomer;
                                                customerSubscription = true;
                                                manageOrderValid = true;
                                                pointsDecision = true;
                                                break;
                                            case "N":
                                                pointsDecision = true;
                                                manageOrderValid = true;
                                                clearScreen(sc);
                                                break;
                                            case "EXIT":
                                                pointsDecision = true;
                                                manageOrderValid = true;
                                                enterOrders = false;
                                                break;
                                            default:
                                                System.out.println("Please Enter Y or N or Exit");
                                                clearScreen(sc);
                                                break;
                                        }
                                    } while (!pointsDecision);
                                    break;
                                case "EXIT":
                                    manageOrderValid = true;
                                    enterOrders = false;
                                    break;
                                default:
                                    System.out.println("Please Enter Y or N or Exit");
                                    break;
                            }
                            /*
                             * make an orderList, add products to orderList, total price, points total for
                             * subscribers
                             */
                            while (enterOrders) {
                                List<iFood> orderList = new ArrayList<iFood>();
                                int pointsTotal = 0;
                                double priceTotal = 0;
                                boolean customerOrder = true;
                                if (customerSubscription) {
                                    do {
                                        System.out.println(ed.displayAllProduct(foodList));
                                        System.out.println(
                                                "Enter the id of the product to add to the order: \n(Type -1 to finish order | -2 Display Current Order List) ");
                                        int orderProdId = 123;
                                        String orderProdIdSTR = sc.nextLine();
                                        iFood orderedFood = null;
                                        try {
                                            orderProdId = Integer.parseInt(orderProdIdSTR);
                                            if (orderProdId > 0) {
                                                try {
                                                    orderedFood = ed.findFood(foodList, orderProdId);
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("No words allowed. Please Try again.");
                                            clearScreen(sc);
                                        }

                                        switch (orderProdId) {
                                            case -1:
                                                customerOrder = false;
                                                enterOrders = false;
                                                break;
                                            case -2:
                                                boolean deleteOrders = true;
                                                if (orderList.size() == 0) {
                                                    printEqualLines();
                                                    System.out.println("Cart is currently EMPTY");
                                                    clearScreen(sc);
                                                    break;
                                                }
                                                do {
                                                    clearScreen(sc);
                                                    System.out.println(ed.displayAllProduct(orderList));
                                                    printEqualLines();
                                                    System.out.println("Do you want to delete an item from the cart?");
                                                    System.out.println("Y/N");
                                                    String deleteDecision = sc.nextLine().toUpperCase();
                                                    switch (deleteDecision) {
                                                        case "Y":

                                                            System.out.println(
                                                                    "Please enter the id of the product you want to remove.");
                                                            int removeItemId = 123;
                                                            String removeItemIdSTR = sc.nextLine();
                                                            iFood removeItem = null;
                                                            try {
                                                                removeItemId = Integer.parseInt(removeItemIdSTR);
                                                                if (removeItemId > 0) {
                                                                    removeItem = ed.findFood(orderList, removeItemId);
                                                                    removeFromOrderList(orderList, removeItem);
                                                                    priceTotal = calculatePrice(removeItem, priceTotal,
                                                                            "Minus");
                                                                    pointsTotal = calculatePoints(removeItem,
                                                                            pointsTotal, "Minus");
                                                                    try {
                                                                        fi.updateProductsFile(foodList);
                                                                        ct.appendCSV(customerList);
                                                                        et.appendCSV(employeeList);
                                                                    } catch (Exception e) {
                                                                        System.out.println(e);
                                                                    }
                                                                    System.out.println(
                                                                            "Successfully removed: \n" + removeItem);
                                                                }
                                                            } catch (Exception e) {
                                                                System.out.println(e);
                                                            }
                                                            break;
                                                        case "N":
                                                            deleteOrders = false;
                                                            break;
                                                        default:
                                                            System.out.println("Invalid please try again!");
                                                            break;
                                                    }
                                                } while (deleteOrders);
                                                clearScreen(sc);
                                                break;

                                            default:
                                                if (orderedFood == null) {
                                                    break;
                                                }
                                                addToOrderList(orderList, orderedFood);
                                                pointsTotal = calculatePoints(orderedFood, pointsTotal, "Add");
                                                priceTotal = calculatePrice(orderedFood, priceTotal, "Add");
                                                try {
                                                    fi.updateProductsFile(foodList);
                                                    ct.appendCSV(customerList);
                                                    et.appendCSV(employeeList);
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                                System.out.println(orderedFood);
                                                clearScreen(sc);
                                                break;
                                        }
                                    } while (customerOrder);
                                    // Display Total, Ask if customer wants a discount if yes, show 10k points 10%
                                    // off
                                    // Finalize
                                    currentCustomer.setPoints(currentCustomer.getPoints() + pointsTotal);
                                    if (currentCustomer.getPoints() > 10000) {
                                        boolean pointsValid = false;
                                        do {
                                            clearScreen(sc);
                                            System.out.println(ed.displayDiscount(currentCustomer.getPoints()));
                                            String pointsDecision = sc.nextLine().toUpperCase();

                                            switch (pointsDecision) {
                                                case "Y":
                                                    priceTotal *= .90;
                                                    // Update Customer points
                                                    currentCustomer.setPoints(currentCustomer.getPoints() - 10000);
                                                    pointsValid = true;
                                                    break;
                                                case "N":
                                                    pointsValid = true;
                                                    break;
                                                default:
                                                    System.out.println("Invalid value please try again");
                                                    break;
                                            }
                                        } while (!pointsValid);

                                    }
                                } else {
                                    do {
                                        System.out.println(ed.displayAllProduct(foodList));
                                        System.out.println(
                                                "Enter the id of the product to add to the order: \n(Type -1 to finish order | -2 Display Current Order List) ");
                                        int orderProdId = 123;
                                        String orderProdIdSTR = sc.nextLine();
                                        iFood orderedFood = null;
                                        try {
                                            orderProdId = Integer.parseInt(orderProdIdSTR);
                                            if (orderProdId > 0) {
                                                try {
                                                    orderedFood = ed.findFood(foodList, orderProdId);
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("No words allowed. Please Try again.");
                                            clearScreen(sc);
                                        }

                                        switch (orderProdId) {
                                            case -1:
                                                customerOrder = false;
                                                enterOrders = false;
                                                break;
                                            case -2:
                                                boolean deleteOrders = true;
                                                if (orderList.size() == 0) {
                                                    printEqualLines();
                                                    System.out.println("Cart is currently EMPTY");
                                                    clearScreen(sc);
                                                    break;
                                                }
                                                do {
                                                    clearScreen(sc);
                                                    System.out.println(ed.displayAllProduct(orderList));
                                                    System.out.println("Do you want to delete an item from the cart?");
                                                    System.out.println("Y/N");
                                                    String deleteDecision = sc.nextLine().toUpperCase();
                                                    switch (deleteDecision) {
                                                        case "Y":

                                                            System.out.println(
                                                                    "Please enter the id of the product you want to remove.");
                                                            int removeItemId = 123;
                                                            String removeItemIdSTR = sc.nextLine();
                                                            iFood removeItem = null;
                                                            try {
                                                                removeItemId = Integer.parseInt(removeItemIdSTR);
                                                                if (removeItemId > 0) {
                                                                    removeItem = ed.findFood(orderList, removeItemId);
                                                                    removeFromOrderList(orderList, removeItem);
                                                                    priceTotal = calculatePrice(removeItem, priceTotal,
                                                                            "Minus");
                                                                    try {
                                                                        fi.updateProductsFile(foodList);
                                                                        ct.appendCSV(customerList);
                                                                        et.appendCSV(employeeList);
                                                                    } catch (Exception e) {
                                                                        System.out.println(e);
                                                                    }
                                                                    System.out.println(
                                                                            "Successfully removed: \n" + removeItem);
                                                                }
                                                            } catch (Exception e) {
                                                                System.out.println(e);
                                                            }
                                                            break;
                                                        case "N":
                                                            deleteOrders = false;
                                                            break;
                                                        default:
                                                            System.out.println("Invalid please try again!");
                                                            clearScreen(sc);
                                                            break;
                                                    }
                                                } while (deleteOrders);
                                                clearScreen(sc);
                                                break;

                                            default:
                                                if (orderedFood == null) {
                                                    break;
                                                }
                                                addToOrderList(orderList, orderedFood);
                                                priceTotal = calculatePrice(orderedFood, priceTotal, "Add");
                                                try {
                                                    fi.updateProductsFile(foodList);
                                                    ct.appendCSV(customerList);
                                                    et.appendCSV(employeeList);
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                                System.out.println(orderedFood);
                                                clearScreen(sc);
                                                break;
                                        }
                                    } while (customerOrder);
                                }
                                try {
                                    if (customerSubscription) {
                                        hm.appendHistory(currentCustomer.getName(), priceTotal);
                                        history = hm.loadHistory();
                                        try {
                                            fi.updateProductsFile(foodList);
                                            ct.appendCSV(customerList);
                                            et.appendCSV(employeeList);
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }
                                    } else {
                                        hm.appendHistory("Anonymous Customer", priceTotal);
                                        history = hm.loadHistory();
                                        try {
                                            fi.updateProductsFile(foodList);
                                            ct.appendCSV(customerList);
                                            et.appendCSV(employeeList);
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }
                                    }
                                } catch (Exception e) {
                                    System.out.println(e);
                                }
                                clearScreen(sc);
                                printEqualLines();
                                if (currentCustomer != null) {
                                    System.out.println("Your Customer is: " + currentCustomer.getName());
                                    System.out.println("Amount to pay: " + priceTotal);
                                } else {
                                    System.out.println("Anonymous customer");
                                    System.out.println("Amount to pay: " + priceTotal);
                                }
                                manageOrderValid = true;
                                enterOrders = false;
                            }
                        } while (!manageOrderValid);
                        mainMenuValid = true;
                        break;
                    case 3:
                        // Show Sales History
                        clearScreen(sc);
                        for (String line : history) {
                            System.out.println(line);
                        }
                        mainMenuValid = true;
                        break;
                    case 0:
                        // Exit
                        close = true;
                        mainMenuValid = true;
                        break;
                    default:
                        System.out.println("Invalid value, please try again");
                        clearScreen(sc);
                        break;
                }
            } while (!mainMenuValid);
        } while (!close);

    }

    /**
     * Clears the console screen and prompts the user to press Enter to continue.
     *
     * @param sc Scanner object to capture the Enter key press.
     */
    private static void clearScreen(Scanner sc) {
        System.out.println("Press Enter to continue: ");
        sc.nextLine();
        System.out.flush();
        System.out.print("\033[H\033[2J");
    }

    /**
     * Prints a line of equal signs as a visual separator for better readability.
     */
    private static void printEqualLines() {
        System.out.println("===================================EMPLOYEE===================================");
    }

    /**
     * Adds a food item to the order list and triggers the purchase process for the
     * item.
     *
     * @param orderList List of iFood objects representing the order.
     * @param food      iFood object to be added to the order list.
     */
    private static void addToOrderList(List<iFood> orderList, iFood food) {
        orderList.add(food);
        food.purchaseItem();
    }

    /**
     * Removes a food item from the order list and triggers the return process for
     * the item.
     *
     * @param orderList List of iFood objects representing the order.
     * @param food      iFood object to be removed from the order list.
     */
    private static void removeFromOrderList(List<iFood> orderList, iFood food) {
        orderList.remove(food);
        food.returnItem();
    }

    /**
     * Calculates the total points based on the given food item, current total
     * points,
     * and the method (Add or Minus).
     *
     * @param food        iFood object for which points are calculated.
     * @param pointsTotal Current total points.
     * @param method      String representing the calculation method ("Add" or
     *                    "Minus").
     * @return The updated total points after the calculation.
     */
    private static int calculatePoints(iFood food, int pointsTotal, String method) {
        if (method.equals("Add")) {
            pointsTotal += food.getStorePoints();

        } else {
            pointsTotal -= food.getStorePoints();
        }
        return pointsTotal;
    }

    /**
     * Calculates the total price based on the given food item, current total price,
     * and the method (Add or Minus).
     *
     * @param food       iFood object for which the price is calculated.
     * @param priceTotal Current total price.
     * @param method     String representing the calculation method ("Add" or
     *                   "Minus").
     * @return The updated total price after the calculation.
     */
    private static double calculatePrice(iFood food, double priceTotal, String method) {
        if (method.equals("Add")) {
            priceTotal += food.getPrice();
        } else {
            priceTotal -= food.getPrice();
        }
        return priceTotal;
    }
}
