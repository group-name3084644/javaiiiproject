
package inventory.Application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import inventory.Display.AdminDisplay;
import inventory.Food.Fruits;
import inventory.Food.Meats;
import inventory.Food.foodImporter;
import inventory.Food.iFood;
import inventory.Sort.showCategoryAsc;
import inventory.Sort.showMostSale;
import inventory.Sort.sortCheapest;
import inventory.Sort.showLessSale;
import inventory.Sort.sortMostExpensive;
import inventory.Users.Customer;
import inventory.Users.CustomerFileTalker;
import inventory.Users.Employee;
import inventory.Users.EmployeeFiletalker;

public class AdminAppl {
    public static void main(String[] args) throws Exception {
        clearScreen();
        AdminDisplay ad = new AdminDisplay();
        foodImporter fi = new foodImporter();
        List<Customer> customerList = new ArrayList<>();
        CustomerFileTalker ct = new CustomerFileTalker();
        List<iFood> foodList = new ArrayList<iFood>();
        List<Employee> employeeList = new ArrayList<Employee>();
        EmployeeFiletalker et = new EmployeeFiletalker();
        try {
            customerList = ct.loadCustomers();
            foodList = fi.ImportFood();
            employeeList = et.loadEmployees();
            System.out.println(ad.dataLoaded());
            sleepTerminal(1000);
            clearScreen();
        } catch (IOException e) {
            System.out.println(ad.dataNotLoaded());
            sleepTerminal(2000);
            clearScreen();
        } catch (Exception e) {
            System.out.println(ad.dataNotLoaded());
            sleepTerminal(2000);
            clearScreen();
        }

        slowPrint(ad.greetUser("\033[35m" + "ADMINISTRATOR" + "\033[0m"), 5);
        sleepTerminal(2000);
        Scanner sc = new Scanner(System.in);
        boolean displaymainmenu = true;
        int answer = 0;
        clearScreen();
        while (displaymainmenu) {
            clearScreen();
            System.out.println(ad.displayMainMenu());
            answer = tryAnswer(sc, ad);
            switch (answer) {
                case 1:
                    // manage foods meats and fruits
                    // ask how to display food menu
                    manageFoodsMenu(ad, foodList, sc, fi);
                    remakeCSV(employeeList, foodList, customerList, fi, et, ct);
                    loadFoodList(foodList, fi, foodList);
                    break;
                case 2:
                    // manage users (customers and employees)
                    manageUsersMenu(ad, customerList, sc, ct, employeeList, et);
                    remakeCSV(employeeList, foodList, customerList, fi, et, ct);
                    loadCustomerList(ad, customerList, ct);
                    break;
                case 3:
                    // make a order for foods
                    processOrderMenu(foodList, sc, ad);
                    remakeCSV(employeeList, foodList, customerList, fi, et, ct);
                    loadFoodList(foodList, fi, foodList);

                    break;
                case 4:
                    // exit main program
                    clearScreen();
                    displaymainmenu = false;
                    break;
                default:
                    clearScreen();
                    System.out.println(ad.validOptionPlease());
                    sleepTerminal(1000);
                    break;
            }
        }

    }

    /*
     * Manage customers or employees menu a submenu of the main menu
     * 
     * @param AdminDisplay ad
     * 
     * @param List<Customer> customerList
     * 
     * @param Scanner sc
     * 
     * @param CustomerFileTalker ct
     * 
     * @param List<Employee> employeeList
     * 
     * @param EmployeeFiletalker et
     * 
     * @return void
     */
    public static void manageUsersMenu(AdminDisplay ad, List<Customer> customerList, Scanner sc, CustomerFileTalker ct,
            List<Employee> employeeList, EmployeeFiletalker et) {
        boolean displayUsersMenu = true;
        int answer;
        while (displayUsersMenu) {
            clearScreen();
            System.out.println(ad.displayManageUsers());
            answer = tryAnswer(sc, ad);
            switch (answer) {
                case 1:
                    // manage customers
                    showCustomerList(ad, customerList, sc, ct);
                    break;
                case 2:
                    // manage employees
                    showEmployeeList(ad, sc, employeeList, et);
                    break;
                case 3:
                    // exit
                    clearScreen();
                    displayUsersMenu = false;
                    break;
                default:
                    break;
            }
        }
    }

    /*
     * manage foods menu a submenu of the main menu
     * 
     * @param AdminDisplay ad
     * 
     * @param List<iFood> foodList
     * 
     * @param Scanner sc
     * 
     * @param foodImporter fi
     * 
     * @return void
     */
    public static void manageFoodsMenu(AdminDisplay ad, List<iFood> foodList, Scanner sc, foodImporter fi) {
        boolean displayfoodmenu = true;
        while (displayfoodmenu) {
            // ask how to display food menu
            int answer = 0; // Initialize the variable answer
            answer = filterByWhichFoodMenu(ad, foodList, answer, displayfoodmenu, sc); // Pass the additional argument
            // go back to main menu if 8 is selected
            if (answer == 8) {
                break;
            }
            // display what food is selected
            int foodIdSelected = whichFoodToSelect(foodList, ad, sc);
            // go back to main menu if -100 is selected
            if (foodIdSelected == -100) {
                break;
            } else {
                try {
                    RemoveOrEditProduct(foodList, fi, sc, ad, foodIdSelected);
                } catch (IOException e) {
                    System.out.println(ad.writeUnsuccessful());
                    sleepTerminal(1000);
                    return;
                }
            }
        }
    }

    /*
     * process order menu a submenu of the main menu for making orders
     * 
     * @param List<iFood> foodList
     * 
     * @param Scanner sc
     * 
     * @param AdminDisplay ad
     * 
     * @return void
     */
    public static void processOrderMenu(List<iFood> foodList, Scanner sc, AdminDisplay ad) {
        boolean displayOrderMenu = true;
        int answer;
        while (displayOrderMenu) {
            clearScreen();
            System.out.println(ad.displayInventory(foodList));
            answer = tryAnswer(sc, ad);
            switch (answer) {
                case -1:
                    // exit to main menu
                    clearScreen();
                    displayOrderMenu = false;
                    break;
                default:
                    break;
            }
            // display what food is selected and make order
            for (iFood food : foodList) {
                if (food.getId() == answer) {
                    clearScreen();
                    System.out.println(ad.makeOrder(food));
                    answer = tryAnswer(sc, ad);
                    if (answer < 0) {
                        break;
                    }
                    food.setStock(food.getStock() + answer);
                    break;
                }
            }
        }
    }

    /*
     * show employee list a submenu of the manage users menu
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @param List<Employee> employeeList
     * 
     * @param EmployeeFiletalker et
     * 
     * @return void
     */
    private static void showEmployeeList(AdminDisplay ad, Scanner sc, List<Employee> employeeList,
            EmployeeFiletalker et) {

        boolean exitToMainMenu = false;
        while (!exitToMainMenu) {
            clearScreen();
            // display employee list and ask if they want to add a new employee or edit an
            System.out.println(ad.displayEmployees(employeeList));
            int answer = tryAnswer(sc, ad);
            if (answer == -1) {
                addNewEmployee(sc, et, ad);
                employeeList = loadEmployeeList(employeeList, et);
                break;
            }
            if (answer == -2) {
                break;
            }
            for (Employee employee : employeeList) {
                if (employee.getId() == answer) {
                    try {
                        manageEmployee(employeeList, employee, ad, sc);
                        break;
                    } catch (Exception e) {
                        System.out.println(ad.writeUnsuccessful());
                        return;
                    }
                }
            }
            break;
        }
    }

    /*
     * manage employee , remove or edit a SINGLE employee
     * 
     * @param List<Employee> employeeList
     * 
     * @param Employee employee
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void manageEmployee(List<Employee> employeeList, Employee employee, AdminDisplay ad, Scanner sc) {
        // delete or edit employee?
        clearScreen();
        System.out.println(ad.displayEmployeeInfo(employee));
        boolean exitToMainMenu = false;
        while (!exitToMainMenu) {
            int answer = tryAnswer(sc, ad);
            if (answer == 1) {
                // delete and update employee list
                employeeList.remove(employee);
                clearScreen();
                System.out.println(ad.removedEmployee(employee));
                sleepTerminal(1000);
                break;
            }
            if (answer == 2) {
                editingEmployee(employee, ad, sc);
                break;
            }
            if (answer == 3) {
                break;
            }
        }
    }

    /*
     * editing employee menu a submenu of the manage employee menu for editing a
     * SINGLE employee
     * 
     * @param Employee employee
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editingEmployee(Employee employee, AdminDisplay ad, Scanner sc) {
        boolean editing = true;
        int answer = 0;
        clearScreen();
        // display what employee is selected
        System.out.println(ad.editingEmployee(employee.getName()));
        sleepTerminal(1000);
        while (editing) {
            clearScreen();
            // display what employee is selected and ask what they want to edit
            System.out.println(ad.displayEditEmployee(employee));
            answer = tryAnswer(sc, ad);
            sc.nextLine(); // consume the newline character is need because
            // sometimes the program breaks when editing different types of data
            switch (answer) {
                case 1:
                    editNameEmployee(employee, ad, sc);
                    break;
                case 2:
                    editContactInfo(employee, ad, sc);
                    break;
                case 3:
                    editPassword(employee, ad, sc);
                    break;
                case 4:
                    clearScreen();
                    editing = false;
                    break;
                default:
                    break;
            }
        }
    }

    /*
     * helper method for editingemployee
     * edit name employee
     * 
     * @param Employee employee
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editNameEmployee(Employee employee, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(employee.getName(), "NAME"));
        System.out.println("Enter the new name: ");
        String newName = sc.nextLine();
        try {
            employee.setName(newName);
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
            return;
        }
    }

    /*
     * helper method for editingemployee
     * edit contact info employee
     * 
     * @param Employee employee
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editContactInfo(Employee employee, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(employee.getName(), "CONTACT INFO"));
        System.out.println("Enter the new contact info: ");
        String newContactInfo = sc.nextLine();
        try {
            // set the new contact info
            employee.setContactInfo(newContactInfo);
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
            return;
        }
    }

    /*
     * helper method for editingemployee
     * edit password employee
     * 
     * @param Employee employee
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editPassword(Employee employee, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(employee.getName(), "PASSWORD"));
        System.out.println("Enter the new password: ");
        String newPassword = sc.nextLine();
        try {
            // set the new password
            employee.setPassword(newPassword);
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
            return;
        }
    }

    /*
     * load food list from csv
     * 
     * @param List<iFood> foodList
     * 
     * @param foodImporter fi
     * 
     * @param List<iFood> newFoodList
     * 
     * @return List<iFood> foodList
     */
    private static List<Employee> loadEmployeeList(List<Employee> employeeList, EmployeeFiletalker et) {
        try {
            // load the new employee list
            List<Employee> newEmployeeList = new ArrayList<>();
            newEmployeeList = et.loadEmployees();
            employeeList.clear();
            employeeList.addAll(newEmployeeList);
        } catch (Exception e) {
            System.out.println("Could not load data.");
        }
        return employeeList;
    }

    /*
     * add new employee
     * 
     * @param Scanner sc
     * 
     * @param EmployeeFiletalker et
     * 
     * @param AdminDisplay ad
     * 
     * @return void
     */
    private static void addNewEmployee(Scanner sc, EmployeeFiletalker et, AdminDisplay ad) {
        try {
            // create a new employee object
            Employee newEmployee = new Employee("Employee", 1, "placeholder", "placeholder", "placeholder");
            System.out.println("Enter employee ID: ");
            newEmployee.setId(sc.nextInt());
            sc.nextLine();
            // ask for the new employee info
            System.out.println("Enter employee name: ");
            newEmployee.setName(sc.nextLine());
            System.out.println("Enter employee contact info: ");
            newEmployee.setContactInfo(sc.nextLine());
            System.out.println("Enter employee password: ");
            newEmployee.setPassword(sc.nextLine());
            et.writeToCSV(newEmployee);
            // add the new employee to the employee list
        } catch (IOException e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
        }
    }

    /*
     * add new customer
     * 
     * @param Scanner sc
     * 
     * @param CustomerFileTalker ct
     * 
     * @param AdminDisplay ad
     * 
     * @return void
     */
    public static void addNewCustomer(Scanner sc, CustomerFileTalker ct, AdminDisplay ad) {
        try {
            // create a new customer object
            Customer newCustomer = new Customer("Customer", 1, "placeHolder", "placeHolder", 500);
            // ask for the new customer info
            System.out.println("Enter customer ID: ");
            newCustomer.setId(sc.nextInt());
            sc.nextLine();
            System.out.println("Enter customer name: ");
            newCustomer.setName(sc.nextLine());
            System.out.println("Enter customer contact info: ");
            newCustomer.setContactInfo(sc.nextLine());
            System.out.println("Enter customer points: ");
            newCustomer.setPoints(sc.nextInt());
            ct.writeToCSV(newCustomer);

        } catch (IOException e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
            sleepTerminal(1000);
        }
    }

    /*
     * show customer list
     * 
     * @param List<Customer> customerList
     * 
     * @param CustomerFileTalker ct
     * 
     * @return List<Customer> customerList
     */
    public static void showCustomerList(AdminDisplay ad, List<Customer> customerList, Scanner sc,
            CustomerFileTalker ct) {
        clearScreen();
        // display customer list and ask if they want to add a new customer or edit an
        // existing one
        System.out.println(ad.displayManageCustomer(customerList));
        boolean exitToMainMenu = false;
        while (!exitToMainMenu) {
            // add a new customer
            int answer = tryAnswer(sc, ad);
            if (answer == -1) {
                addNewCustomer(sc, ct, ad);
                customerList = loadCustomerList(ad, customerList, ct);
                break;
            }
            // exit to main menu
            if (answer == -2) {
                break;
            }
            // display what customer is selected
            for (Customer customer : customerList) {
                if (customer.getId() == answer) {
                    manageCustomer(customerList, customer, ad, sc);
                    break;
                }
            }
            break;
        }
    }

    /*
     * lets the user edit the food item
     * 
     * @param List<iFood> foodList
     * 
     * @param int foodIdSelected
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     */
    public static void displayEditOrders(iFood food, AdminDisplay ad, Scanner sc) {
        boolean editing = true;
        int answer = 0;
        // display what food is selected
        while (editing) {
            clearScreen();
            System.out.println(ad.displayEditOrders(food));
            answer = tryAnswer(sc, ad);
            sc.nextLine(); // consume the newline character
            // edit the food item
            switch (answer) {
                case 1:
                    editNameFood(food, ad, sc);
                    break;
                case 2:
                    editQuantityEach(food, ad, sc);
                    break;
                case 3:
                    editPrice(food, ad, sc);
                    break;
                case 4:
                    editBrand(food, ad, sc);
                    break;
                case 5:
                    editStock(food, ad, sc);
                    break;
                case 6:
                    editStorePoints(food, ad, sc);
                    break;
                case 7:
                    editSale(food, ad, sc);
                    break;
                case 8:
                    // exit
                    clearScreen();
                    editing = false;
                    break;
                default:
                    clearScreen();
                    System.out.println(ad.displayEditOrders(food));
                    sleepTerminal(1000);
                    break;
            }
        }
    }

    /*
     * manage the customer , remove or edit
     * 
     * @param List<Customer> customerList
     * 
     * @param Customer customer
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    public static void manageCustomer(List<Customer> customerlist, Customer customer, AdminDisplay ad, Scanner sc) {
        // delete or edit?
        clearScreen();
        System.out.println(ad.displayCustomerInfo(customer));
        boolean exitToMainMenu = false;
        while (!exitToMainMenu) {
            int answer = tryAnswer(sc, ad);
            if (answer == 1) {
                // delete and update
                customerlist.remove(customer);
                clearScreen();
                System.out.println(ad.removedCustomer(customer));
                sleepTerminal(1000);
                break;
            }
            // edit customer
            if (answer == 2) {
                editingCustomer(customer, ad, sc);
                break;
            }
            // exit to main menu
            if (answer == 3) {
                break;
            }
        }
    }

    /*
     * editing customer menu
     * 
     * @param Customer customer
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    public static void editingCustomer(Customer customer, AdminDisplay ad, Scanner sc) {
        boolean editing = true;
        int answer = 0;
        clearScreen();
        // display what customer is selected
        System.out.println(ad.editingCustomer(customer.getName()));
        sleepTerminal(1000);
        while (editing) {
            clearScreen();
            System.out.println(ad.displayEditCustomer(customer));
            answer = tryAnswer(sc, ad);
            sc.nextLine(); // consume the newline character
            // edit the customer
            switch (answer) {
                case 1:
                    editNameCustomer(customer, ad, sc);
                    break;
                case 2:
                    editContactInfo(customer, ad, sc);
                    break;
                case 3:
                    editPoints(customer, ad, sc);
                    break;
                case 4:
                    // exit to main menu
                    clearScreen();
                    editing = false;
                    break;
                default:
                    break;
            }
        }
    }

    /*
     * remove or edit a product
     * 
     * @param List<iFood> foodList
     * 
     * @param foodImporter fi
     * 
     * @param Scanner sc
     * 
     * @param AdminDisplay ad
     * 
     * @param int foodIdSelected
     * 
     * @return void
     */
    public static void RemoveOrEditProduct(List<iFood> foodList, foodImporter fi, Scanner sc, AdminDisplay ad,
            int foodIdSelected) throws IOException {
        int answer = 0;
        boolean displayfoodmenu = true;
        // display what food is selected and ask if they want to remove or edit
        while (displayfoodmenu) {
            answer = tryAnswer(sc, ad);
            switch (answer) {
                case 1:
                    clearScreen();
                    try {
                        // remove the food item
                        removeProduct(foodList, foodIdSelected, ad);
                        displayfoodmenu = false;
                        break;
                    } catch (ConcurrentModificationException e) {
                        // idk why this exception is thrown but if i catch it everything works normally
                        return;
                    }
                    // edit the food item
                case 2:
                    clearScreen();
                    editProduct(foodList, foodIdSelected, ad, sc);
                    fi.updateProductsFile(foodList);
                    displayfoodmenu = false;
                    break;
                // exit to main menu
                case 3:
                    clearScreen();
                    displayfoodmenu = false;
                    return;

                default:
                    sc.nextLine();
                    break;
            }
        }
    }

    /*
     * this is a method that displays the food menu and allows the user to select
     * a food item
     * 
     * @param List<iFood> foodList
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return int answer the user selected
     */
    public static int whichFoodToSelect(List<iFood> foodList, AdminDisplay ad, Scanner sc) {
        int answer = 0;
        boolean loop = true;
        while (loop) {
            answer = tryAnswer(sc, ad);
            for (iFood food : foodList) {
                if (answer == food.getId()) {
                    clearScreen();
                    System.out.println(ad.displayManageOrders(food));
                    return answer;
                }
            }
            // exit to main menu
            if (answer == -2) {
                return -100;
            }
            // display what food is selected
            if (answer == -1) {
                addNewProduct(foodList, ad, sc);
                // add new product method
                return -100;
            } else {
                sc.nextLine();
            }
        }
        return answer;
    }

    /*
     * add new product method
     * 
     * @param List<iFood> foodList
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    public static void addNewProduct(List<iFood> foodList, AdminDisplay ad, Scanner sc) {
        try {
            // create a new food object
            clearScreen();
            System.out.println(ad.addingNewProduct());
            sleepTerminal(1000);
            sc.nextLine();
            // Get input for the new product
            System.out.println(ad.askTypeProduct());
            String type = tryAnswerString(sc, ad);

            System.out.println(ad.askIdProduct());
            int id = tryAnswer(sc, ad);
            sc.nextLine();
            System.out.println(ad.askNameProduct());
            String name = tryAnswerString(sc, ad);

            System.out.println(ad.askQuantityEachProduct());
            int quantity = tryAnswer(sc, ad);

            System.out.println(ad.askPriceProduct());
            double price = tryAnswerDouble(sc, ad);
            sc.nextLine();
            System.out.println(ad.askBrandProduct());
            String brand = tryAnswerString(sc, ad);
            // points
            System.out.println(ad.askStorePointsProduct());
            int storePoints = tryAnswer(sc, ad);
            // Create a new iFood object with the input values

            if (type.equals("Fruit")) {
                iFood newFood = new Fruits(type, id, name, quantity, price, brand, 0, storePoints, 0);
                foodList.add(newFood);
            }
            if (type.equals("Meat")) {
                iFood newFood = new Meats(type, id, name, quantity, price, brand, 0, storePoints, 0);
                foodList.add(newFood);
            }
        } catch (IOException e) {
            System.out.println(ad.errorAddingProduct());
            return;
        } catch (Exception e) {
            System.out.println(ad.errorAddingProduct());
            return;
        }
        System.out.println(ad.successAddingProduct());
        sleepTerminal(1000);
    }

    /*
     * filter by what the user selects for foods
     * 
     * @param AdminDisplay ad
     * 
     * @param List<iFood> foodList
     * 
     * @param int answer
     * 
     * @param boolean displayfoodmenu
     * 
     * @param Scanner sc
     * 
     * @return the answer the user selected
     */
    public static int filterByWhichFoodMenu(AdminDisplay ad, List<iFood> foodList, int answer,
            boolean displayfoodmenu, Scanner sc) {
        boolean loop = true;

        while (loop) {
            clearScreen();
            System.out.println(ad.displayFoodMenu());
            answer = tryAnswer(sc, ad);
            switch (answer) {
                case 1:
                    // display food menu
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new showCategoryAsc()));
                    loop = false;
                    break;
                case 2:
                    // most sales
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new showMostSale()));
                    loop = false;
                    break;
                case 3:
                    // less sales
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new showLessSale()));
                    loop = false;
                    break;
                case 4:
                    // highest price
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new sortMostExpensive()));
                    loop = false;
                    break;
                case 5:
                    // cheapest price
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new sortCheapest()));
                    loop = false;
                    break;
                case 6:
                    // alphabetical asc
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new showCategoryAsc()));
                    loop = false;
                    break;
                case 7:
                    // alphabetical dsc
                    clearScreen();
                    System.out.println(ad.displaySortedFoodMenu(foodList, new showCategoryAsc()));
                    loop = false;
                    break;
                case 8:
                    // exit
                    clearScreen();
                    loop = false;
                    return 8;
                default:
                    clearScreen();
                    System.out.println(ad.validOptionPlease());
                    sleepTerminal(1000);
                    break;
            }
        }
        return answer;
    }

    // HELPER METHODS//
    // remake csv
    /*
     * remake the csv
     * 
     * @param List<Employee> employeeList
     * 
     * @param List<iFood> foodList
     * 
     * @param List<Customer> customerList
     * 
     * @param foodImporter fi
     * 
     * @param EmployeeFiletalker et
     * 
     * @param CustomerFileTalker ct
     * 
     * @return void
     */
    public static void remakeCSV(List<Employee> employeeList, List<iFood> foodList, List<Customer> customerList,
            foodImporter fi, EmployeeFiletalker et, CustomerFileTalker ct) throws IOException {
        fi.updateProductsFile(foodList);
        ct.appendCSV(customerList);
        et.appendCSV(employeeList);
    }

    /*
     * This method is used to try to get an int from the user
     * 
     * @return int
     */
    public static int tryAnswer(Scanner sc, AdminDisplay ad) {
        int answer = 0;
        try {
            answer = sc.nextInt();
        } catch (InputMismatchException e) {
            sc.nextLine();
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
            return -100;
        }
        return answer;
    }

    // try for double
    /*
     * This method is used to try to get a double from the user
     * 
     * @return double
     */
    public static double tryAnswerDouble(Scanner sc, AdminDisplay ad) {
        double answer = 0;
        try {
            answer = sc.nextDouble();
        } catch (InputMismatchException e) {
            sc.nextLine();
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
            return -100;
        }
        return answer;
    }

    /*
     * This method is used to try to get a string from the user
     * 
     * @return String
     */
    public static String tryAnswerString(Scanner sc, AdminDisplay ad) {
        String answer = "";
        while (answer.length() == 0) {
            answer = sc.nextLine();
            if (answer.length() == 0) {
                ad.validOptionPlease();
                sleepTerminal(1000);
            }
        }
        return answer;
    }

    /*
     * This method is used to clear the screen
     * 
     * @return void
     */
    public static void clearScreen() {
        System.out.flush();
        System.out.print("\033[H\033[2J");
    }

    /*
     * helper method for displayEditOrders
     * edit name food
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editNameFood(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "NAME"));
        String updateToString = sc.nextLine();
        food.setName(updateToString);
    }

    /*
     * helper method for displayEditOrders
     * edit quantity each
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editQuantityEach(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "QUANTITY EACH"));
        int updateToInt = tryAnswer(sc, ad);

        if (updateToInt < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            food.setQuantityEach(updateToInt);
        }
    }

    /*
     * helper method for displayEditOrders
     * edit price
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editPrice(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "PRICE"));
        double updateToDouble = tryAnswerDouble(sc, ad);

        if (updateToDouble < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            food.setPrice(updateToDouble);
        }
    }

    /*
     * helper method for displayEditOrders
     * edit brand
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editBrand(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "BRAND"));
        String updateToString = sc.nextLine();
        food.setBrand(updateToString);
    }

    /*
     * helper method for displayEditOrders
     * edit stock
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editStock(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "STOCK"));
        int updateToInt = tryAnswer(sc, ad);

        if (updateToInt < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            food.setStock(updateToInt);
        }
    }

    /*
     * helper method for displayEditOrders
     * edit store points
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editStorePoints(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "STORE POINTS"));
        int updateToInt = tryAnswer(sc, ad);

        if (updateToInt < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            food.setStorePoints(updateToInt);
        }
    }

    /*
     * helper method for displayEditOrders
     * edit sale
     * 
     * @param iFood food
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editSale(iFood food, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(food.getName(), "SALE"));
        int updateToInt = tryAnswer(sc, ad);

        if (updateToInt < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            food.setSale(updateToInt);
        }
    }

    /*
     * helper method for editingCustomer
     * edit name customer
     * 
     * @param Customer customer
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editNameCustomer(Customer customer, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(customer.getName(), "NAME"));
        String updateToString = sc.nextLine();
        customer.setName(updateToString);
    }

    /*
     * helper method for editingCustomer
     * edit contact info customer
     * 
     * @param Customer customer
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editContactInfo(Customer customer, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(customer.getName(), "CONTACT INFO"));
        String updateToString = sc.nextLine();
        customer.setContactInfo(updateToString);
    }

    /*
     * helper method for editingCustomer
     * edit points customer
     * 
     * @param Customer customer
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editPoints(Customer customer, AdminDisplay ad, Scanner sc) {
        clearScreen();
        System.out.println(ad.displayWhichEdit(customer.getName(), "POINTS"));
        int updateToInt = tryAnswer(sc, ad);

        if (updateToInt < 0) {
            System.out.println(ad.validOptionPlease());
            sleepTerminal(1000);
        } else {
            customer.setPoints(updateToInt);
        }
    }

    /*
     * helper method for RemoveOrEditProduct
     * remove product
     * 
     * @param List<iFood> foodList
     * 
     * @param int foodIdSelected
     * 
     * @param AdminDisplay ad
     * 
     * @return void
     */
    private static void removeProduct(List<iFood> foodList, int foodIdSelected, AdminDisplay ad) {
        for (iFood food : foodList) {
            if (food.getId() == foodIdSelected) {
                try {
                    foodList.remove(food);
                    System.out.println(ad.removedProduct(food));
                    sleepTerminal(1000);
                } catch (ConcurrentModificationException e) {
                    System.out.println(ad.errorRemoveProduct());
                    return;
                }
            }
        }
    }

    /*
     * helper method for RemoveOrEditProduct
     * edit product
     * 
     * @param List<iFood> foodList
     * 
     * @param int foodIdSelected
     * 
     * @param AdminDisplay ad
     * 
     * @param Scanner sc
     * 
     * @return void
     */
    private static void editProduct(List<iFood> foodList, int foodIdSelected, AdminDisplay ad, Scanner sc) {
        for (iFood food : foodList) {
            if (food.getId() == foodIdSelected) {
                System.out.println(ad.editingProduct(food));
                sleepTerminal(1000);
                displayEditOrders(food, ad, sc);
            }
        }
    }

    /*
     * load customer list
     * 
     * @param List<Customer> customerList
     * 
     * @param CustomerFileTalker ct
     * 
     * @return List<Customer> customerList
     */
    public static List<Customer> loadCustomerList(AdminDisplay ad, List<Customer> customerList, CustomerFileTalker ct) {
        try {
            List<Customer> newCustomerList = new ArrayList<>();
            newCustomerList = ct.loadCustomers();
            customerList.clear();
            customerList.addAll(newCustomerList);
        } catch (IOException e) {
            System.out.println(ad.writeUnsuccessful());
        } catch (Exception e) {
            System.out.println(ad.writeUnsuccessful());
        }
        return customerList;
    }

    /*
     * load food list
     * 
     * @param List<iFood> foodList
     * 
     * @param foodImporter fi
     * 
     * @param List<iFood> foodList2
     * 
     * @return List<iFood> foodList
     */
    public static List<iFood> loadFoodList(List<iFood> foodList, foodImporter fi, List<iFood> foodList2) {
        try {
            List<iFood> newFoodList = new ArrayList<>();
            newFoodList = fi.ImportFood();
            foodList.clear();
            foodList.addAll(newFoodList);
        } catch (Exception e) {
            System.out.println("Could not load data.");
        }
        return foodList;
    }

    /*
     * sleep terminal
     * 
     * @param int milisec
     * 
     * @return void
     */
    public static void sleepTerminal(int milisec) {
        try {
            Thread.sleep(milisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*
     * slow print
     * 
     * @param String str
     * 
     * @param int milisec
     * 
     * @return void
     */
    public static void slowPrint(String str, int milisec) {
        str.chars().forEach(c -> {
            System.out.print((char) c);
            try {
                Thread.sleep(milisec);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
