package inventory.Sort;

import inventory.Food.iFood;

public class showMostSale implements iSorter {

    @Override
    public int sortFoods(iFood one, iFood two) {
        return two.getSale() - one.getSale();
    }

}
