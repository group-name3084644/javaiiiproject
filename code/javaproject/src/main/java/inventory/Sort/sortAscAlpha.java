package inventory.Sort;

import inventory.Food.iFood;

public class sortAscAlpha implements iSorter {
    @Override
    public int sortFoods(iFood one, iFood two) {
        return one.getName().compareTo(two.getName());
    }
}
