package inventory.Sort;

import inventory.Food.iFood;

public class sortMostExpensive implements iSorter {
    @Override
    public int sortFoods(iFood one, iFood two) {
        double priceDifference = two.getPrice() - one.getPrice();
        return (int) priceDifference;
    }
}
