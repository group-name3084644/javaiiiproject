package inventory.Sort;

import inventory.Food.iFood;

public interface iSorter {
    public int sortFoods(iFood one, iFood two);
}
