package inventory.Sort;

import inventory.Food.iFood;

public class sortCheapest implements iSorter {
    @Override
    public int sortFoods(iFood one, iFood two) {
        double priceDifference = one.getPrice() - two.getPrice();
        return (int) priceDifference;
    }
}
