package inventory.Sort;

import inventory.Food.iFood;

public class sortDescAlpha implements iSorter {
    @Override
    public int sortFoods(iFood one, iFood two) {
        return two.getName().compareTo(one.getName());
    }

}
