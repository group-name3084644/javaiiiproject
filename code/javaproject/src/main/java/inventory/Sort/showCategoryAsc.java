package inventory.Sort;

import inventory.Food.iFood;

public class showCategoryAsc implements iSorter {

    @Override
    public int sortFoods(iFood one, iFood two) {
        return one.getTypename().compareTo(two.getTypename());

    }
}
