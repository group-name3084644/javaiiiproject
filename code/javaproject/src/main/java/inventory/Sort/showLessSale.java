package inventory.Sort;

import inventory.Food.iFood;

public class showLessSale implements iSorter{

    @Override
    public int sortFoods(iFood one, iFood two) {
        return one.getSale() - two.getSale();
    }
}
