package inventory.History;

import java.nio.file.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class HistoryFileManager {
    public List<String> loadHistory() throws IOException{
        String filePath = "data/StoreSalesHistory.txt";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)){
            throw new IOException();
        }
        List<String> lines = Files.readAllLines(pathToFile);
        return lines;
    }

    public void appendHistory(String customer, double price) throws IOException{
        String filePath = "data/StoreSalesHistory.txt";
        Path pathToFile = Paths.get(filePath);
        if(!Files.exists(pathToFile)){
            throw new IOException();
        }
        try (BufferedWriter writer = Files.newBufferedWriter(pathToFile, StandardOpenOption.APPEND)){
            writer.write("Customer: " + customer);
            writer.newLine();
            writer.write("Amount Paid: " + price);
            writer.newLine();
            writer.write("============================");
            writer.newLine();
        } catch (IOException e){
            e.printStackTrace();
        }
    } 
}
