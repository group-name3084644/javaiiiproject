package inventory.Food;

import java.util.List;

public interface iFoodImporter {

    // This is a interace for FruitImporter and MeatImporter
    // List<Fruits> loadFruits(String pathFile) throws IOException;
    List<iFood> ImportFood() throws NumberFormatException, Exception;
    // List<Meats> LoadMeats();

}
