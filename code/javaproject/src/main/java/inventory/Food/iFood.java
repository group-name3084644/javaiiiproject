package inventory.Food;

public interface iFood {

    public int getId();

    public void setName(String newName);

    public String getTypename();

    public String getName();

    public int getQuantityEach();

    public double getWeight();

    public double getPrice();

    public String getBrand();

    public int getStock();

    public int getStorePoints();

    public int getSale();

    public String printObj(int indexOf);

    public void setStock(int newStock);

    public void purchaseItem();

    public void returnItem();

    public boolean equals(Object obj);

    public void setQuantityEach(int nextInt);

    public void setPrice(double nextDouble);

    public void setBrand(String nextLine);

    public void setStorePoints(int nextInt);

    public void setSale(int nextInt);

}
