package inventory.Food;

public class Meats implements iFood {
    private int id;
    private String typename;
    private String name;
    private double weight;
    private double price;
    private String brand;
    private int stock;
    private int storePoints;
    private int sale;

    public Meats(Meats meat) throws Exception {
        this(meat.typename, meat.id, meat.name, meat.weight, meat.price, meat.brand, meat.stock,
                meat.storePoints, meat.sale);
    }

    public Meats(String typename, int productId, String name, double weight, double price,
            String brand, int stock, int storePoints, int sale) {
        if (name.equals("") || weight < 0 || price < 0 || brand.equals("") || stock < 0 || storePoints < 0) {
            throw new IllegalArgumentException("invalid vlaues detected");
        }
        this.typename = typename;
        this.id = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.brand = brand;
        this.stock = stock;
        this.storePoints = storePoints;
        this.sale = sale;
    }

    public String toString() {
        return "Id: " + this.id + " | Name: " + this.name + " | Brand: " + this.brand + " | Price: " + this.price;
    }

    public String getTypename() {
        return this.typename;
    }
    
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getQuantityEach() {
        return (int) this.weight;
    }

    public double getWeight() {
        return this.weight;
    }

    public double getPrice() {
        return this.price;
    }

    public String getBrand() {
        return this.brand;
    }
    
    public int getStock() {
        return this.stock;
    }

    public int getStorePoints() {
        return this.storePoints;
    }

    public int getSale() {
        return this.sale;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public void setQuantityEach(int nextInt) {
        this.weight = nextInt;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setStock(int newStock) {
        this.stock = newStock;
    }
    
    @Override
    public void setStorePoints(int nextInt) {
        this.storePoints = nextInt;
    }

    @Override
    public void setSale(int nextInt) {
        this.sale = nextInt;
    }
    
    public void purchaseItem() {
        this.stock -= 1;
        this.sale += 1;
    }

    public void returnItem() {
        this.stock += 1;
        this.sale -= 1;
    }

    @Override
    public String printObj(int indexOf) {
        return String.format("--" + name.toUpperCase() + "-ID-" + this.id + "--\n" +
                "  WEIGHT:" + this.weight + "\n" +
                "  BRAND:" + this.brand + "\n" +
                "  PRICE:" + this.price + "\n" +
                "  STOCK:" + this.stock + "\n" +
                "  SALES:" + this.sale + "\n" +
                "----------------\n");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Meats)) {
            return false;
        }
        Meats meat = (Meats) obj;
        return meat.id == this.id;
    }

}
