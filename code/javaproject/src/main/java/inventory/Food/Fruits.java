package inventory.Food;

public class Fruits implements iFood {
    private String typename;
    private int id;
    private String name;
    private int quantityEach;
    private double price;
    private String brand;
    private int stock;
    private int storePoints;
    private int sale;

    public Fruits(String typename, int id, String name, int quantityEach, double price,
            String brand, int stock, int storePoints, int sale) throws Exception {
        if (name.equals("") || quantityEach < 0 || price < 0 || brand.equals("")) {
            throw new IllegalArgumentException("invalid vlaues detected");
        }
        this.typename = typename;
        this.id = id;
        this.name = name;
        this.quantityEach = quantityEach;
        this.price = price;
        this.brand = brand;
        this.stock = stock;
        this.storePoints = storePoints;
        this.sale = sale;
    }

    public String printObj(int number) {
        return String.format("--" + name.toUpperCase() + "-ID-" + this.id + "--\n" +
                "  QUANTITYPERUNIT:" + this.quantityEach + "\n" +
                "  BRAND:" + this.brand + "\n" +
                "  PRICE:" + this.price + "\n" +
                "  STOCK:" + this.stock + "\n" +
                "  SALES:" + this.sale + "\n" +
                "----------------\n");
    }

    @Override
    public String toString() {
        return "Id: " + this.id + " | Name: " + this.name + " | Brand: " + this.brand + " | Price: " + this.price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        if (price < 0) {
            throw new IllegalArgumentException("invalid price");
        }
        this.price = price;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTypename() {
        return this.typename;
    }

    public int getQuantityEach() {
        return this.quantityEach;
    }

    public double getWeight() {
        throw new UnsupportedOperationException("Fruits Have quantity not weight");
    }

    public int getStock() {
        return this.stock;
    }

    public int getStorePoints() {
        return this.storePoints;
    }

    public int getSale() {
        return this.sale;
    }

    public void setStock(int newStock) {
        this.stock = newStock;
    }

    public void purchaseItem() {
        this.stock -= this.quantityEach;
        this.sale += 1;
    }

    public void returnItem() {
        this.stock += this.quantityEach;
        this.sale -= 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Fruits)) {
            return false;
        }
        Fruits fruit = (Fruits) obj;
        return fruit.id == this.id;
    }

    @Override
    public void setQuantityEach(int nextInt) {
        this.quantityEach = nextInt;
    }

    @Override
    public void setPrice(double nextDouble) {
        this.price = nextDouble;
    }

    @Override
    public void setStorePoints(int nextInt) {
        this.storePoints = nextInt;
    }

    @Override
    public void setSale(int nextInt) {
        this.sale = nextInt;
    }
}
