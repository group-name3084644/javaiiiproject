package inventory.Food;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class sqlFoodImporter implements iFoodImporter, SQLData {

    @Override
    public List<iFood> ImportFood() throws NumberFormatException, Exception {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        List<iFood> foodList = new ArrayList<iFood>();
        try {
            System.out.println("what is your username?");
            String user = System.console().readLine();
            System.out.println("what is your password?");
            String password = System.console().readLine();
            conn = getConnection(user, password);
            System.out.println("Connected to database");
            // get all foods
            stmt = conn.prepareStatement("{call my_package.getProduct()}");
            stmt.executeQuery();
            rs = stmt.getResultSet();
            while (rs.next()) {
                if (rs.getString("TYPE").equals("Fruit")) {
                    Fruits fruit = new Fruits(rs.getString("TYPE"), rs.getInt("ID"), rs.getString("NAME"),
                            rs.getInt("QUANTITYEACH"), rs.getDouble("PRICE"), rs.getString("BRAND"), rs.getInt("STOCK"),
                            rs.getInt("STOREPOINTS"), rs.getInt("SALE"));
                    foodList.add(fruit);
                }
                if (rs.getString("TYPE").equals("Meat")) {
                    Meats meat = new Meats(rs.getString("TYPE"), rs.getInt("ID"), rs.getString("NAME"),
                            rs.getDouble("WEIGHT"), rs.getDouble("PRICE"), rs.getString("BRAND"), rs.getInt("STOCK"),
                            rs.getInt("STOREPOINTS"), rs.getInt("SALE"));
                    foodList.add(meat);
                }
            }
            return foodList;
        } catch (Exception e) {

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return foodList;
    }

    public static Connection getConnection(String user, String password) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getSQLTypeName'");
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'readSQL'");
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'writeSQL'");
    }
}
