//this is a importer that will take all types of food and return them. Not sure if we need this but we have it.
package inventory.Food;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class foodImporter implements iFoodImporter {
    public List<iFood> ImportFood() throws NumberFormatException, Exception {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        List<String> list = Files.readAllLines(pathToFile);
        List<iFood> foodList = new ArrayList<iFood>();

        for (String lines : list) {
            // check if thing contains a category of fruit if yes append
            String[] data = lines.split(",");
            if (data[0].equals("Fruit")) {
                Fruits fruit = new Fruits(data[0], Integer.parseInt(data[1]), data[2], Integer.parseInt(data[3]),
                        Double.parseDouble(data[4]),
                        data[5], Integer.parseInt(data[6]), Integer.parseInt(data[7]), Integer.parseInt(data[8]));
                foodList.add(fruit);
            }
            if (data[0].equals("Meat")) {
                Meats meat = new Meats(data[0], Integer.parseInt(data[1]), data[2], Double.parseDouble(data[3]),
                        Double.parseDouble(data[4]),
                        data[5], Integer.parseInt(data[6]), Integer.parseInt(data[7]), Integer.parseInt(data[8]));
                foodList.add(meat);
            }
        }

        return foodList;
    }

    public void updateProductsFile(List<iFood> foodList) throws IOException {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        List<String> content = new ArrayList<String>();
        for (iFood food : foodList) {
            if (food.getTypename().equals("Meat")) {
                Meats foodIn = (Meats) food;
                content.add(foodIn.getTypename() + "," + foodIn.getId() + "," + foodIn.getName() + ","
                        + foodIn.getWeight() + "," + foodIn.getPrice() + "," +
                        foodIn.getBrand() + "," + foodIn.getStock() + "," + foodIn.getStorePoints() + ","
                        + foodIn.getSale());
            } else if (food.getTypename().equals("Fruit")) {
                Fruits foodIn = (Fruits) food;
                content.add(foodIn.getTypename() + "," + foodIn.getId() + "," + foodIn.getName() + ","
                        + foodIn.getQuantityEach() + "," + foodIn.getPrice() + "," +
                        foodIn.getBrand() + "," + foodIn.getStock() + "," + foodIn.getStorePoints() + ","
                        + foodIn.getSale());
            }
        }
        Files.write(pathToFile, content);
    }
}
