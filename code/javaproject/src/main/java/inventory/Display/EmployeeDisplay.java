package inventory.Display;

import java.util.List;

import inventory.Food.iFood;
import inventory.Sort.iSorter;
import inventory.Users.Customer;

/**
 * Purpose: This class is used by the Employee Application. 
 * Most of the menu ui is contained within this class.
 * This class also contains and manages the sorting of the product data.
 * It also contains some helper methods that are used inside Employee Application.
 * 
 * @author Alexander Roxas
 * @version 12-02-2023
 */
public class EmployeeDisplay implements iDisplay {

    /**
     * Simple function that greets the user everytime the application is ran.
     * 
     * @param username The username of the user and is displayed.
     * @return The greet message string.
     */
    @Override
    public String greetUser(String username) {
        String greeting = "Welcome " + username + " to the Food Inventory System";
        greeting += "\nPlease choose an option from the menu below";
        return greeting;
    }

    /**
     * Displays the main menu options for the application.
     * 
     * @return The main menu string
     */
    @Override
    public String displayMainMenu() {
        String menu = "===================================EMPLOYEE===================================\n";
        menu += "1 - Display Foods | 2 - Manage Orders | 3 - Show Sales History | 0 - Exit \n";
        menu += "Please choose an option from the menu \n";
        return menu;
    }

    /**
     * Displays the menu for food items sorting menu.
     * 
     * @return The food menu string.
     */
    @Override
    public String displayFoodMenu() {
        String foodMenu = "===================================EMPLOYEE===================================\n";
        foodMenu += "1 - Food Type | 2 - MostSales | 3 - LessSales | 4 - Highest Price | 5 - Cheapest Price | 6 - AlphabeticalAsc | 7 - AlphabeticalDsc | 0 - Go Back \n";
        foodMenu += "Sort by (1-7)? \n";
        return foodMenu;
    }

    /**
     * Displays an option between meats and fruits when selecting food type from the displayFoodMenu().
     * 
     * @return The food type menu string.
     */
    public String displayFoodType() {
        String foodType = "===================================EMPLOYEE===================================\n";
        foodType += "1 - Meats | 2 - Fruits | 0 - Go Back \n";
        foodType += "Please enter the number of the food type you want to search";
        return foodType;
    }

    /**
     * Displays the sorted food menu based on the selected sorting chosen from displayFoodMenu().
     * 
     * @param foodList  The list of food items.
     * @param sortType  The sorting strategy.
     * @return The sorted food string.
     */
    @Override
    public String displaySortedFoodMenu(List<iFood> foodList, iSorter sortType) {
        String sortedMenu = "===================================EMPLOYEE===================================\n";
        selectionSort(foodList, sortType);
        for (iFood food : foodList){
            sortedMenu += food.printObj(0);
        }
        return sortedMenu;
    }

    @Override
    public String displayManageOrders(iFood food) {
        //Input a food and verify if it can be ordered?
        throw new UnsupportedOperationException("Method not Implemented nor used.");
    }

    @Override
    public String displayManageUsers() {
        throw new UnsupportedOperationException("Method for Admins.");
    }

    /**
     * Displays all of the customers and an option to choose a customer currently
     * shopping with the application
     * 
     * @param customerList  The list of customers that are subscribed to the points system.
     * @return a string of the list of customers and a prompt to input the customerId 
     */
    @Override
    public String displayManageCustomer(List<Customer> customerList) {
        String manageCustomerMenu = "===================================EMPLOYEE===================================\n";
        for (Customer customer : customerList){
            manageCustomerMenu += customer.toString();
        }
        manageCustomerMenu += "\nChoose the Customer that is currently shopping with us today! (Id: )";
        return manageCustomerMenu;
    }

    /**
     * Displays the customer info of the customer.
     * 
     * @param customer  The customer to be displayed.
     * @return customer info string
     */
    @Override
    public String displayCustomerInfo(Customer customer) {
        String customerInfo = "===================================EMPLOYEE===================================\n";
        customerInfo += customer.toString();
        return customerInfo;
    }

    /**
     * Displays all of the products that are in the inventory.
     * 
     * @param foodList  The list of foods to be displayed
     * @return a string of all the product info
     */
    public String displayAllProduct(List<iFood> foodList){
        String products = "===================================EMPLOYEE===================================\n";
        for (iFood food : foodList){
            products += food + "\n";
        }
        return products;
    }

    /**
     * Displays all food products of a specific type in the inventory
     * 
     * @param foodList  The list of foods to be displayed
     * @param type      The type of food to be displayed
     * @return  A string of all the product info from the specific type
     */
    public String printAllProductByType(List<iFood> foodList, String type){
        String returned = "===================================EMPLOYEE===================================\n";
        for (iFood food : foodList){
            if(food.getTypename().equals(type)){
                returned += food.printObj(0);
            }
        }
        return returned;
    }
    
    /**
     * Finds a food item in the inventory based on its ID.
     * 
     * @param foodList  The list of foods to be searched
     * @param id        The ID of the food to be displayed
     * @return A string of the food that was found
     * @throws Exception IF no food item with the id is found
     */
    public iFood findFood(List<iFood> foodList, int id) throws Exception{
        for (iFood food : foodList){
            if (food.getId() == id){
                return food;
            }
        }
        throw new Exception("No Food of this ID found");
    }

    /**
     * Finds a customer in the customer list based on their ID.
     * 
     * @param customerList  The list of customers to be searched
     * @param id            The ID of the customer to be displayed
     * @return A string of the Cutomer that was found.
     * @throws Exception IF no customer with the id is found.
     */
    public Customer findCustomer(List<Customer> customerList, int id) throws Exception{
        for(Customer customer : customerList){
            if (customer.getId() == id){
                return customer;
            }
        }
        throw new Exception("no Customers found");
    }

    /**
     * Displays a message about available discounts based on the customer points.
     * (Initially there was supposed to be multiple discount options but ran out of time.)
     * 
     * @param points the points of the customer
     * @return A string with the information about the available discounts.
     */
    public String displayDiscount(int points){
        String ui = "===================================EMPLOYEE===================================\n";
        ui += "Does your customer want to use their points for discounts? \n";
        ui += "They are eligible for a 10% DISCOUNT!!! \n";
        ui += "They have \"" + points + "\" points! \n";
        ui += "Y/N";
        return ui;
    }

    //The remaining methods are private and are used specifically inside this class.

    /**
     * Sorts the list of food items using the specified sorter.
     * 
     * @param foodList  The list of food items to be sorted.
     * @param sorter    The sorting strategy.
     */
    private static void selectionSort(List<iFood> foodList, iSorter sorter){
        for (int i = 0; i < foodList.size() -1; i++){
            int index = i;
            for (int j = i + 1; j < foodList.size(); j++) {
                if (sorter.sortFoods(foodList.get(j), foodList.get(i)) < 0){
                    index = j;
                }
            }
            swap(foodList, i, index);
        }
    }
    /**
     * Swaps two elements in the list.
     * 
     * @param foodList  The list of food items.
     * @param i         The index of the first element.
     * @param j         The index of the second element.
     */
    private static void swap(List<iFood> foodList, int i, int j){
        iFood temp = foodList.get(i);
        foodList.set(i, foodList.get(j));
        foodList.set(j, temp);
    }
}
