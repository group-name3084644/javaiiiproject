package inventory.Display;

import java.util.List;

import inventory.Food.iFood;
import inventory.Sort.iSorter;
import inventory.Users.Customer;

public interface iDisplay {
    public String greetUser(String username);

    // display menu is the main menu for the program depending on the user different
    // options will be displayed
    public String displayMainMenu();

    // display food is a submenu for Main Menu , it will display the food menu
    // asking which type of sort they want
    public String displayFoodMenu();

    // display sorted food menu is a submenu for Main Menu
    // display all the food by what the user chooses to sort by
    // for Admins it will display the food with a delete, edit option, see stock
    // for Customers it will display the food with a add to cart option
    public String displaySortedFoodMenu(List<iFood> foodList, iSorter sortType);

    // manage Orders is a submenu for Main Menu, it will display the shopping cart
    // with a add or remove ui at the bottom.
    // displaying all the food in the shopping cart, with a remove option for
    // customers
    // for admins it will instead display the food with the option to edit the stock
    public String displayManageOrders(iFood food);

    // manage users is a sub menu for Main Menu, it will display a list of users and
    // when you choose a user it will display their information
    public String displayManageUsers();

    // display manage customer is shared by Admins and Customers
    // Manage customer is a submenu for manage users, it will display a list of
    // customers name and when you choose a customer it will display their points
    // and information
    public String displayManageCustomer(List<Customer> customerList);

    // displayCustomerinfo is a submenu for displayManageCustomer admins and
    // customers
    // both can delete and edit the information
    // displaying the specific customer information chosen by the user, Name,
    // Points,contact address, Delete?, Edit?
    public String displayCustomerInfo(Customer c);

}
