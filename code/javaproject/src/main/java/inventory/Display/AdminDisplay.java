package inventory.Display;

import java.util.List;
import inventory.Food.iFood;
import inventory.Sort.iSorter;
import inventory.Users.Customer;
import inventory.Users.Employee;

public class AdminDisplay implements iDisplay {

    /*
     * This class is responsible for displaying the UI for greeting the user
     * 
     * @return String
     */
    @Override
    public String greetUser(String username) {
        String greeting = "Welcome back esteemed " + username
                + "\nYou have been granted exclusive access to our state-of-the-art Food Inventory Management System.";
        return greeting;
    }

    /*
     * This class is responsible for displaying the UI for the main menu
     * 
     * @return String
     */
    @Override
    public String displayMainMenu() {
        String menu = "=========================================ADMIN===========================================\n";
        menu = "1. Manage Foods | 2. Manage Users | 3. Make Order | 4. Exit";
        menu += "\nPlease choose an option from the menu (1-3)";
        return menu;
    }

    /*
     * This class is responsible for displaying the UI for the food menu
     * 
     * @return String
     */
    @Override
    public String displayFoodMenu() {
        String foodMenu = "=========================================ADMIN===========================================\n";
        foodMenu += "1. Category | 2. MostSales | 3. LessSales | 4. Highest Price | 5. Cheapest Price | 6. AlphabeticalAsc | 7. AlphabeticalDsc | 8. Exit";
        foodMenu += "Sort by (1-8)?\n";
        return foodMenu;
    }

    /*
     * This class is responsible for displaying the UI for the sorted food menu
     * 
     * @param foodList
     * 
     * @param sortType
     * 
     * @return String
     */
    @Override
    public String displaySortedFoodMenu(List<iFood> foodList, iSorter sortType) {
        String foodMenu = "=========================================ADMIN===========================================\n";
        // sort the food list
        selectionSort2(foodList, sortType);
        for (iFood food : foodList) {
            foodMenu += adminToString(food) + "\n";
        }
        foodMenu += "Select food Id(?) | Add New Food(-1) | Exit (-2)";
        return foodMenu;
    }

    /*
     * This class is responsible for displaying the UI for the manage food menu
     * 
     * @param food
     * 
     * @return String
     */
    @Override
    public String displayManageOrders(iFood food) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += adminToString(food) + "\n";
        ui += "1. Remove Product | 2. Edit Product | 3. Exit";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the make order menu
     * 
     * @param food
     * 
     * @return String
     */
    public String makeOrder(iFood food) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "Id: " + food.getId() + "| Name: " + food.getName() + " |Stock: " + food.getStock() + "\n";
        ui += "How many would you like to order?\n";
        return ui;
    }

    /*
     * display the UI for the editting food
     */
    public String displayEditOrders(iFood food) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += adminToString(food) + "\n\n\n";
        ui += "Edit which?\n1. Name | 2. QuantityEach | 3. Price | 4. Brand | 5. Stock | 6. StorePoints | 7. Sale. | 8. Exit";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the manage customer menu
     * 
     * @param Customer
     * 
     * @return String
     */
    public String displayEditCustomer(Customer customer) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += customer + "\n\n\n";
        ui += "Edit which?\n1. Name | 2. Contact | 3. Points | 4. Exit";
        return ui;
    }

    /*
     * prints out the food list
     * 
     * @param Food
     * 
     * @return String
     */
    public String adminToString(iFood food) {
        String ui = food + "| QuantityEach: " + food.getQuantityEach() + "| Brand: " + food.getBrand() + "| Stock: "
                + food.getStock() + "| StorePoints: "
                + food.getStorePoints() + "| Sale: " + food.getSale() + "\n";

        return ui;
    }

    /*
     * this class is diplayed when editing something
     */
    public String displayWhichEdit(String name, String option) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "What new " + option + " would you like to change " + name + " to?\n";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the manage users menu
     * 
     * @return String
     */
    @Override
    public String displayManageUsers() {
        String ui = "=========================================ADMIN===========================================\n";
        ui = "1. Customer | 2. Employees | 3.Exit \n";
        ui += "Which user type would you like to manage?\n";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the manage customer menu
     * 
     * @param List<Customer>
     * 
     * @return String
     */
    @Override
    public String displayManageCustomer(List<Customer> customerList) {
        String ui = "=========================================ADMIN===========================================\n";
        for (Customer customer : customerList) {
            ui += customer;
        }
        ui += "Choose A Customer Id To Manage | -1. Add New Customer | -2. Exit ";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the manage customer menu
     * 
     * @param Customer
     * 
     * @return String
     */
    @Override
    public String displayCustomerInfo(Customer c) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += c + "\n";
        ui += "1. Delete Customer | 2. Edit Customer | 3. Exit";
        return ui;
    }

    /*
     * This class dispaly employees
     * 
     * @param List<Employee>
     * 
     * @return String
     */
    public String displayEmployees(List<Employee> employeeList) {
        String ui = "=========================================ADMIN===========================================\n";
        for (Employee e : employeeList) {
            ui += e;
        }
        ui += "Choose A Customer Id To Manage | -1. Add New Employee | -2. Exit";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for the manage employee menu
     * 
     * @param Employee
     * 
     * @return String
     */
    public String displayEmployeeInfo(Employee employee) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += employee + "\n";
        ui += "1. Delete Employee | 2. Edit Employee | 3. Exit";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user removes a
     * product
     * 
     * @param iFood
     * 
     * @return String
     */
    public String removedProduct(iFood food) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "SUCCESSFULLY REMOVED PRODUCT " + food.getName() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user makes a invalid
     * option
     * 
     * @return String
     */
    public String validOptionPlease() {
        return "\033[31mValid option please\033[0m";
    }

    /*
     * This class is responsible for displaying the UI for when user makes a edit on
     * a food
     * 
     * @param iFood
     * 
     */
    public String editingProduct(iFood food) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "EDITING PRODUCT " + food.getName() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user adds a new
     * customer
     * 
     * @param Customer
     * 
     * @return String
     */
    public String editingCustomer(String name) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "EDIIING CUSTOMER \033[31m" + name.toUpperCase() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user adds a new
     * product
     * 
     * @return String
     */
    public String addingNewProduct() {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "ADDING PRODUCT NEW PRODUCT" + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user loads data
     * 
     * @return String
     */
    public String dataLoaded() {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[32m" + "DATA LOADED SUCCESSFULLY" + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user loads data
     * unsuccessfully
     * 
     * @return String
     */
    public String dataNotLoaded() {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "UNSUCCESSFUL DATA IMPORT" + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when user writes data
     * 
     * @return String
     */
    public String writeUnsuccessful() {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "UNSUCCESSFUL DATA WRITE" + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for sorting the food list
     * 
     * @return String
     */
    public static void selectionSort2(List<iFood> foodList, iSorter sorter) {
        for (int i = 0; i < foodList.size() - 1; i++) {
            int index = i;
            for (int j = i + 1; j < foodList.size(); j++) {
                if (sorter.sortFoods(foodList.get(j), foodList.get(index)) < 0) {
                    index = j;
                }
            }
            swap(foodList, i, index);
        }
    }

    /*
     * This class is responsible for displaying the UI for when removes a customer
     * 
     * @param Customer
     * 
     * @return String
     */
    public String removedCustomer(Customer customer) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "REMOVED CUSTOMER " + customer.getName() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when editing a employee
     * 
     * @param customer
     * 
     * @return String
     */
    public String editingCustomer(Customer customer) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "EDITING CUSTOMER " + customer.getName() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for help swapping the food list
     * 
     * @return void
     */
    public static void swap(List<iFood> list, int i, int j) {
        iFood temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }

    /*
     * This class is responsible for displaying the UI for when removing a employee
     * 
     * @param Employee
     * 
     * @return String
     */
    public String removedEmployee(Employee employee) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "REMOVED EMPLOYEE " + employee.getName() + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when editing a employee
     * 
     * @param String
     * 
     * @return String
     */
    public String editingEmployee(String name) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "EDITING EMPLOYEE " + name + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when adding a new
     * employee
     * 
     * @return String
     */
    public String addingNewEmployee() {
        String ui = "=========================================ADMIN===========================================\n";
        ui += "\033[31m" + "ADDING NEW EMPLOYEE" + "\033[0m";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for when editing a employee
     * 
     * @return String
     */
    public String displayEditEmployee(Employee employee) {
        String ui = "=========================================ADMIN===========================================\n";
        ui += employee + "\n\n\n";
        ui += "Edit which?\n1. Name | 2. Contact | 3. Password | 4. Exit";
        return ui;
    }

    /*
     * This class is responsible for displaying the UI for displaying the inventory
     * 
     * @param List<iFood>
     * 
     * @return String
     */
    public String displayInventory(List<iFood> foodList) {
        String ui = "=========================================ADMIN===========================================\n";
        for (iFood food : foodList) {
            ui += "Id: " + food.getId() + "| Name: " + food.getName() + " |Stock: " + food.getStock() + "\n";
        }
        ui += "Select food Id(?) | Exit (-1)";
        return ui;
    }

    /*
     * ask the user for the type of product
     * 
     * @return String
     */
    public String askTypeProduct() {
        return "Enter the type of the product: ";
    }

    /*
     * ask the user for the id of the product
     * 
     * @return String
     */
    public String askIdProduct() {
        return "Enter the id of the product: ";
    }

    /*
     * ask the user for the name of the product
     * 
     * @return String
     */
    public String askNameProduct() {
        return "Enter the name of the product: ";
    }

    /*
     * ask the user for the quantity of the product
     * 
     * @return String
     */
    public String askQuantityEachProduct() {
        return "Enter the quantity of the product: ";
    }

    /*
     * ask the user for the price of the product
     * 
     * @return String
     */
    public String askPriceProduct() {
        return "Enter the price of the product: ";
    }

    /*
     * ask the user for the brand of the product
     * 
     * @return String
     */
    public String askBrandProduct() {
        return "Enter the brand of the product: ";
    }

    /*
     * ask the user for the stock of the product
     * 
     * @return String
     */
    public String askStockProduct() {
        return "Enter the stock of the product: ";
    }

    /*
     * ask the user for the store points of the product
     * 
     * @return String
     */
    public String askStorePointsProduct() {
        return "Enter the store points of the product: ";
    }

    /*
     * error adding product
     * 
     * @return String
     */
    public String errorAddingProduct() {
        return "Error adding product";
    }

    /*
     * success adding product
     * 
     * @return String
     */
    public String successAddingProduct() {
        return "Successfully added new product!";
    }

    /*
     * error removing product
     * 
     * @return String
     */
    public String errorRemoveProduct() {
        return "Error removing product";
    }
}
