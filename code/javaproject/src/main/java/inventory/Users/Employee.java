package inventory.Users;

public class Employee {
    // follow the same pattern as Customer.java
    private String typeName;
    private int id;
    private String name;
    private String password;
    private String contactInfo;

    public Employee(String typeName, int id, String name, String contactInfo, String password) {
        if (name.equals("") || contactInfo.equals("") || password.equals("") || id < 0) {
            throw new IllegalArgumentException("invalid vlaues detected");
        }
        this.typeName = typeName;
        this.id = id;
        this.name = name;
        this.contactInfo = contactInfo;
        this.password = password;
    }

    public Employee(String string, int i, Object object, int j, int k, String string2, int l, int m, int n) {
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getContactInfo() {
        return this.contactInfo;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setId(int nextInt) {
        this.id = nextInt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactInfo(String contact) {
        this.contactInfo = contact;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return "Id: " + this.id + " Name: " + this.name + " Contact Info: " + this.contactInfo + " Password: "
                + this.password + "\n";
    }
}
