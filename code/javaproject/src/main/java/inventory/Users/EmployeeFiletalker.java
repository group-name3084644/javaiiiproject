package inventory.Users;

import java.nio.file.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class EmployeeFiletalker {
    // load employees
    // make a method that will add a new employee to the csv file
    // make a method that will append the csv file with the new employees
    public List<Employee> loadEmployees() throws IOException, Exception {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        List<String> lines = Files.readAllLines(pathToFile);
        List<Employee> employeeList = new ArrayList<Employee>();

        for (String line : lines) {
            String[] data = line.split(",");

            if (data[0].equals("Employee")) {
                Employee employee = new Employee(data[0], Integer.parseInt(data[1]), data[2], data[3],
                        data[4]);
                employeeList.add(employee);
            }
        }
        return employeeList;
    }

    public void writeToCSV(Employee newEmployee) throws IOException, Exception {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        try (BufferedWriter writer = Files.newBufferedWriter(pathToFile, StandardOpenOption.APPEND)) {
            writer.write(newEmployee.getTypeName() + "," + newEmployee.getId() + "," + newEmployee.getName() + ","
                    + newEmployee.getContactInfo() + "," + newEmployee.getPassword());
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendCSV(List<Employee> employeeList) throws IOException {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }

        List<String> lines = new ArrayList<>();
        for (Employee employee : employeeList) {
            String line = employee.getTypeName() + "," + employee.getId() + "," + employee.getName() + ","
                    + employee.getContactInfo() + "," + employee.getPassword();
            lines.add(line);
        }
        Files.write(pathToFile, lines, StandardOpenOption.APPEND);
    }
}
