package inventory.Users;

public class Customer {
    private int id;
    private String typeName;
    private String name;
    private String contactInfo;
    private int points;

    public Customer(String typeName, int id, String name, String contactInfo, int points) {

        if (name.equals("") || contactInfo.equals("") || points < 0 || id < 0) {
            throw new IllegalArgumentException("invalid vlaues detected");
        }
        this.id = id;
        this.typeName = typeName;
        this.name = name;
        this.contactInfo = contactInfo;
        this.points = points;

    }

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getName() {
        return name;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public int getPoints() {
        return points;
    }

    public void setCustomerId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Id: " + this.id + " Name: " + this.name + " Contact Info: " + this.contactInfo + " Points: "
                + this.points + "\n";
    }

    public void setId(int nextInt) {
        this.id = nextInt;
    }
}
