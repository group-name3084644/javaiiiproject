package inventory.Users;

import java.nio.file.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class CustomerFileTalker {
    public List<Customer> loadCustomers() throws IOException, Exception {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        List<String> lines = Files.readAllLines(pathToFile);
        List<Customer> customerList = new ArrayList<Customer>();

        for (String line : lines) {
            String[] data = line.split(",");

            if (data[0].equals("Customer")) {
                Customer customer = new Customer(data[0], Integer.parseInt(data[1]), data[2], data[3],
                        Integer.parseInt(data[4]));
                customerList.add(customer);
            }
        }
        return customerList;
    }

    public void writeToCSV(Customer newCustomer) throws IOException, Exception {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }
        try (BufferedWriter writer = Files.newBufferedWriter(pathToFile, StandardOpenOption.APPEND)) {
            writer.write(newCustomer.getTypeName() + "," + newCustomer.getId() + "," + newCustomer.getName() + ","
                    + newCustomer.getContactInfo() + "," + newCustomer.getPoints());
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // make a method that will rewrite the csv file with the new customer list
    public void replaceCSV(List<Customer> customerList) throws IOException {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }

        List<String> lines = new ArrayList<>();
        for (Customer customer : customerList) {
            String line = customer.getTypeName() + "," + customer.getId() + "," + customer.getTypeName() + ","
                    + customer.getName() + ","
                    + customer.getContactInfo() + "," + customer.getPoints();
            lines.add(line);
        }

        Files.write(pathToFile, lines);
    }

    // append a list of customers to the csv file
    public void appendCSV(List<Customer> customerList) throws IOException {
        String filePath = "data/SampleData.csv";
        Path pathToFile = Paths.get(filePath);
        if (!Files.exists(pathToFile)) {
            throw new IOException();
        }

        List<String> lines = new ArrayList<>();
        for (Customer customer : customerList) {
            String line = customer.getTypeName() + "," + customer.getId() + "," + customer.getName() + ","
                    + customer.getContactInfo() + "," + customer.getPoints();
            lines.add(line);
        }
        Files.write(pathToFile, lines, StandardOpenOption.APPEND);
    }
}
